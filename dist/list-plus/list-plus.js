
//获取应用实例
const app = getApp()
// const i18n = require('../../utils/i18n')
const util = require('../../utils/util')
// const langData = require('./langData')
import event from '../../utils/event'

import data from '../../utils/cityData'

Component({
  relations: {
    '../list/index': {
      type: 'parent', // 关联的目标节点应为子节点
      linked(target) {
        // 每次有custom-li被插入时执行，target是该节点实例对象，触发在该节点attached生命周期之后
      },
      linkChanged(target) {
        // 每次有custom-li被移动后执行，target是该节点实例对象，触发在该节点moved生命周期之后
      },
      unlinked(target) {
        // 每次有custom-li被移除时执行，target是该节点实例对象，触发在该节点detached生命周期之后
      }
    }
  },

  options: {
    multipleSlots: true
  },
  externalClasses: ['l-class', 'l-class-icon', 'l-class-image', 'l-class-right', 'l-class-content', 'l-class-desc'],
  properties: {
    icon: String,
    righticon: String,
    image: String,
    title: String,
    desc: String,
    title1: String,
    b_data: String,
    tagPosition: {
      type: String,
      value: 'left'
    },
    tagContent: String,
    tagShape: {
      type: String,
      value: 'square'
    },
    tagColor: String,
    tagPlain: Boolean,
    badgePosition: {
      type: String,
      value: 'left'
    },
    dotBadge: Boolean,
    badgeCount: Number,
    badgeMaxCount: {
      type: Number,
      value: 99
    },
    badgeCountType: {
      type: String,
      value: 'overflow'
    },
    date: String,
    rightDesc: String,
    rightCountry: String,
    gap: Number,
    leftGap: Number,
    rightGap: Number,
    languages: {
      type: Object,
      value: null
    },
    langIndex: {
      type: Number,
      value: 0
    },
    shengao: String,
    sgIndex: Number,
    shengaos: Object,
    isLink: {
      type: Boolean,
      value: true,
    },
    linkType: {
      type: String,
      value: 'navigateTo'
    },
    url: String

  },
  // 使用 Object.assign 补充翻译的 data
  data: {
    // language: '',
    // languages: ['简体中文', 'English'],
    // langIndex: 0,
    options1: data,
    value1: [],
    // showSkeleton: true
    // date: util.formatDate(new Date())
    // ...
  },
  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
      // // 代理小程序的 setData，在更新数据后，翻译传参类型的字符串
      // util.resetSetData.call(this, langData)
      // this.setData({
      //   langIndex: wx.getStorageSync('langIndex')
      // });
      // this.setLanguage();
      // setTimeout(() => {
      //   this.setData({
      //     showSkeleton: false
      //   })
      // }, 2000)
    },
    moved: function () { },
    detached: function () { },

  },
  methods: {
    tapcell: function (e) {
      const {
        linkType,
        url
      } = e.currentTarget.dataset;
      if (url) {
        wx[linkType]({
          url
        });
      }
      this.triggerEvent('lintap', {
        e
      }, {})
    },
    // bindCountryChange: function (e) {
    //   let index = e.detail.value;
    //   this.setData({	// (1)
    //     langIndex: index
    //   });
    //   wx.T.setLocaleByIndex(index);
    //   this.setLanguage();
    //   event.emit('languageChanged');

    //   wx.setStorage({
    //     key: 'langIndex',
    //     data: this.data.langIndex
    //   })
    // },
    bindShenGaoChange: function (e) {
      let index = e.detail.value;
      this.setData({	// (1)
        sgIndex: index
      });

      this.triggerEvent('change', { index }, {})
    },
    bindDateChange: function (e) {
      let index = e.detail.value;
      this.setData({
        date: e.detail.value
      })
      this.triggerEvent('change', { index }, {})
    },
    onOpen1() {
      console.log("onOpen1")
      this.setData({ visible1: true })
    },
    onClose1() {
      this.setData({ visible1: false })
    },
    onChange1(e) {
      this.setData({ title1: e.detail.options.map((n) => n.label).join('-') })
      console.log('onChange1', e.detail)
    },
    // setLanguage() {
    //   this.setData({
    //     language: wx.T.getLanguage()
    //   });
    // }
  }
});