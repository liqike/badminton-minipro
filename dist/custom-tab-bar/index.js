import event from '../../utils/event'
Component({
  properties: {
    position: {
      type: String,
      value: 'bottom'
    },
    show: {
      type: Boolean,
      value: true
    },
    selected: {
      type: Number,
      value: 1
    },
    color: {
      type: String,
      value: '#707070'
    },
    selectedColor: {
      type: String,
      value: '3963BC'
    },
    borderStyle: {
      type: String,
      value: '#f6f6f6'
    },
    backgroundColor: {
      type: String,
      value: '#fff'
    },
    backgroundImg: {
      type: String,
      value: ''
    },
    fontSize: {
      type: Number,
      value: 24
    },
    isRedirectToTab: {
      type: Boolean,
      value: true
    },
    // 是否跳转
    isNav: {
      type: Boolean,
      value: true
    },
    list: {
      type: Array,
      value: []
    }
  },
  data: {
    language: {},
  },
  attached() {
    this.setLanguage();	// (1)
    event.on("languageChanged", this, this.setLanguage); // (2)
  },

  methods: {
    switchTab(e) {
      const data = e.currentTarget.dataset
      // const url = data.path
      // console.log(e)
      // if (this.data.isNav) {
      //   if (this.data.isRedirectToTab) {
      //     wx.switchTab({
      //       url
      //     })
      //   } else {
      //     wx.switchTab({
      //       url
      //     })
      //   }
      // }
      var index = data.index
      // 使用 triggerEvent 方法触发自定义组件事件，指定事件名、detail对象和事件选项
      this.triggerEvent('okEvent', { index }, {})

      // this.showItem(data.index)
    },
    show() {
      this.setData({
        show: true
      })
    },
    hide() {
      this.setData({
        show: false
      })
    },
    showItem(idx) {
      this.setData({
        selected: idx
      })
      let detail = {
        idx,
        path:this.route
      };
      let option = {};
      this.triggerEvent('lintap', detail, option);
    },
    showRedDot(idx) {
      const redDot = `list[${idx}].redDot`
      this.setData({
        [redDot]: true
      })
    },
    hideRedDot(idx) {
      const redDot = `list[${idx}].redDot`
      this.setData({
        [redDot]: false
      })
    },
    setTabBarBadge(idx, text) {
      const badge = `list[${idx}].badge`
      this.setData({
        [badge]: text
      })
    },
    removeTabBarBadge(idx) {
      const badge = `list[${idx}].badge`
      this.setData({
        [badge]: ''
      })
    },
    setLanguage() {
      this.setData({
        language: wx.T.getLanguage()
      });
    },

  }
})