// input/input.js
import rules from '../behaviors/rules';
const map = require("../../utils/map.js");
const util = require("../../utils/util.js");
const date = new Date();
const years = [];
const months = [];
const days = [];
const hours = [];
const minutes = [];
//获取年
for (let i = date.getFullYear() - 2; i <= date.getFullYear() + 5; i++) {
  years.push("" + i + "年");
}
//获取月份
for (let i = 1; i <= 12; i++) {
  if (i < 10) {
    i = "0" + i;
  }
  months.push("" + i + "月");
}
//获取日期
for (let i = 1; i <= 31; i++) {
  if (i < 10) {
    i = "0" + i;
  }
  days.push("" + i + "日");
}
//获取小时
for (let i = 0; i < 24; i++) {
  if (i < 10) {
    i = "0" + i;
  }
  hours.push("" + i + "时");
}
//获取分钟
for (let i = 0; i < 60; i++) {
  if (i < 10) {
    i = "0" + i;
  }
  minutes.push("" + i + "分");
}
Component({
  /**
   * 组件的属性列表
   */
  options: {
    multipleSlots: true,
    addGlobalClass: true,
  },
  behaviors: ['wx://form-field', rules],
  externalClasses: ['l-class', 'l-label-class','l-error-text'],
  properties: {
    datas:Object,
    index: Number,
    city:String,
    image:String,
    address:String,
    checked: String,
    date: String,
    startTime:String,
    week: String,
    icon: String,
    // 表单标题（label）的文本
    label: {
      type: String,
      value: ''
    },
    // 是否自定义label部分
    labelCustom: {
      type: Boolean,
      value: false
    },
    // 是否显示下划线
    showRow: {
      type: Boolean,
      value: true
    },
    // 是否必选
    required: {
      type: Boolean,
      value: false
    },
    // 占位文本
    placeholder: {
      type: String,
      value: ''
    },
    // 输入框类型
    type: {
      type: String,
      value: 'text'
    },
    // 输入框的值
    value: {
      type: String,
      value: ''
    },
    // 是否需要冒号
    colon: {
      type: Boolean,
      value: false
    },
    // 获取焦点
    focus: {
      type: Boolean,
      value: false
    },
    // 是否显示清除按钮
    clear: {
      type: Boolean,
      value: false
    },
    right:{
      type: Boolean,
      value: false
    },
    // 是否显示日期
    isDate: {
      type: Boolean,
      value: false
    },
    // 是否显示日期
    isDateTime: {
      type: Boolean,
      value: false
    },
    // 是否显示地理位置
    isMap: {
      type: Boolean,
      value: false
    },
    is_input: {
      type: Boolean,
      value: false
    },
    // 是否显示地理位置
    is_single: {
      type: Boolean,
      value: false
    },
    is_slider:{
      type: Boolean,
      value: false
    },
    is_switch:{
      type: Boolean,
      value: false
    },
    // 最大输入长度
    maxlength: {
      type: Number,
      value: 140
    },
    // 表单项的宽度，单位rpx
    width: {
      type: Number,
      value: 750
    },
    // 表单项标题部分的宽度，单位rpx
    labelWidth: {
      type: Number,
      value: 200
    },
    // label标题的显示位置 left top right
    labelLayout: {
      type: String,
      value: 'left'
    },
    // 是否禁用
    disabled: {
      type: Boolean,
      value: false
    },
    // 占位文字的样式  
    placeholderStyle: {
      type: String,
      value: ''
    },
    minValue: {
      type: Number,
      value: 0
    },
    maxValue: {
      type: Number,
      value: 100
    },
    showValue:{
      type: Number,
      value: 30
    },
    homeScore: {
      type: Number,
      value: 0
    },
    awayScore: {
      type: Number,
      value: 0
    },
    is_derc:{
      type: Boolean,
      value: false
    },
    is_douPicker:{
      type: Boolean,
      value: false
    },
    is_images:{
      type: Boolean,
      value: false
    },
    images:{
      type: Object,
      value: []
    },
    sArray:{
      type: Object,
      value: []
    },
    sIndex:{
      type: Object,
      value: [0, 0, 0]
    },
    derc: {
      type: String,
      value: ''
    },
    remark: {
      type: String,
      value: ''
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    sIndex:[0,0,0],
    value:'',
    startTime: '',
    endTime: '',
    multiArray: [years, months, days, hours, minutes],
    multiIndex: [],
    choose_year: '',
  },
  attached() {
    this.initRules();
   
  },
  /**
   * 组件的方法列表
   */
  methods: {

    bindDateChange: function (e) {
      let index = e.detail.value;
      this.setData({
        date: e.detail.value
      })
      this.triggerEvent('change', { index }, {})
    },
    bindDataChange(e){
      let index = e.detail.value;
      // this.setData({
      //   index: index
      // })
      this.triggerEvent('dataChange', { index }, {})
    },
    handleInputChange(event) {
      // console.log(event)
      const {
        detail = {}
      } = event;
      const {
        value = ''
      } = detail;

      this.setData({
        value
      });

      this.triggerEvent('linchange', event);
    },

    handleInputFocus(event) {
      this.triggerEvent('linfocus', event);
    },

    handleInputBlur(event) {
      this.validatorData({
        value: event.detail.value
      });
      this.triggerEvent('linblur', event);
    },
    handleInputConfirm(event) {
      const {
        detail = {}
      } = event;
      const {
        value = ''
      } = detail;

      this.setData({
        value
      });

      this.triggerEvent('linconfirm', event);
    },
    onClearTap(event) {
      this.setData({
        value: ''
      })
      this.triggerEvent('linclear', event);
    },
    sliderchange(e) {
      this.triggerEvent('sliderchange', e.detail.value);
    },
   
    switchChange(){
      this.triggerEvent('switchChange');
    },
    onclickTap(){
      this.triggerEvent('onclickTap');
    },
    bindMultiPickerChange(e){
      console.log(this.data.sArray)
      console.log(e)
      var left = this.data.sArray[0][e.detail.value[0]]
      var right = this.data.sArray[2][e.detail.value[2]]
      var value = this.data.sArray[0][e.detail.value[0]] + this.data.remark + this.data.sArray[2][e.detail.value[2]]
      this.setData({
        sIndex: e.detail.value,
        value: value
      })
      this.triggerEvent('multiPickerChange', { left, right});
    },
    startMulti() {
      var left = this.data.sArray[0][this.data.homeScore]
      var right = this.data.sArray[2][this.data.awayScore]
      var value = this.data.sArray[0][this.data.homeScore] + this.data.remark + this.data.sArray[2][this.data.awayScore]
      var index = [this.data.homeScore, 0, this.data.awayScore]
      this.setData({
        sIndex: index,
        value: value
      })
    },
    bindMultiPickerColumnChange:function(e){
       
    },
    //获取开始时间日期
    bindMultiPickerChangeStart: function (e) {
      var startTime = this.setbindMultiPickerData(e)
      var week = util.getWeek(startTime.replace(/-/g, '/'))
      this.setData({
        startTime: startTime,
        week: week
      })
      this.triggerEvent('getTime', {startTime,week});
    },
    //获取结束时间日期
    bindMultiPickerChangeEnd: function (e) {
      // console.log('picker发送选择改变，携带值为', e.detail.value)
      var endTime = this.setbindMultiPickerData(e)
      this.setData({
        endTime: endTime
      })
    },
    //监听picker的滚动事件
    bindMultiPickerColumnChangeStart: function (e) {
      this.getbindMultiPickerData(e)
    },
    bindMultiPickerColumnChangeEnd: function (e) {
      this.getbindMultiPickerData(e)
    },
    setbindMultiPickerData: function (e) {


      this.setData({
        multiIndex: e.detail.value
      })
      const index = this.data.multiIndex;
      const year = this.data.multiArray[0][index[0]];
      const month = this.data.multiArray[1][index[1]];
      const day = this.data.multiArray[2][index[2]];
      const hour = this.data.multiArray[3][index[3]];
      const minute = this.data.multiArray[4][index[4]];
      // console.log(`${year}-${month}-${day}-${hour}-${minute}`);

      return year.replace("年", "") + '-' + month.replace("月", "") + '-' + day.replace("日", "") + ' ' + hour.replace("时", "") + ':' + minute.replace("分", "") 

    },
    getbindMultiPickerData: function (e) {
      //获取年份
      if (e.detail.column == 0) {
        let choose_year = this.data.multiArray[e.detail.column][e.detail.value];
        this.setData({
          choose_year
        })
      }
      //console.log('修改的列为', e.detail.column, '，值为', e.detail.value);
      if (e.detail.column == 1) {
        let num = parseInt(this.data.multiArray[e.detail.column][e.detail.value]);
        let temp = [];
        if (num == 1 || num == 3 || num == 5 || num == 7 || num == 8 || num == 10 || num == 12) { //判断31天的月份
          for (let i = 1; i <= 31; i++) {
            if (i < 10) {
              i = "0" + i;
            }
            temp.push("" + i + "日");
          }
          this.setData({
            ['multiArray[2]']: temp
          });
        } else if (num == 4 || num == 6 || num == 9 || num == 11) { //判断30天的月份
          for (let i = 1; i <= 30; i++) {
            if (i < 10) {
              i = "0" + i;
            }
            temp.push("" + i + "日");
          }
          this.setData({
            ['multiArray[2]']: temp
          });
        } else if (num == 2) { //判断2月份天数
          let year = parseInt(this.data.choose_year);

          if (((year % 400 == 0) || (year % 100 != 0)) && (year % 4 == 0)) {
            for (let i = 1; i <= 29; i++) {
              if (i < 10) {
                i = "0" + i;
              }
              temp.push("" + i + "日");
            }
            this.setData({
              ['multiArray[2]']: temp
            });
          } else {
            for (let i = 1; i <= 28; i++) {
              if (i < 10) {
                i = "0" + i;
              }
              temp.push("" + i + "日");
            }
            this.setData({
              ['multiArray[2]']: temp
            });
          }
        }
      }
      var data = {
        multiArray: this.data.multiArray,
        multiIndex: this.data.multiIndex
      };
      data.multiIndex[e.detail.column] = e.detail.value;
      this.setData(data);
    },
    startShowTime: function () {
      var date = this.data.startTime
      this.changeTime(date)
    },
    endShowTime: function () {
      var date = this.data.endTime
      this.changeTime(date)
    },

    changeTime: function (dates) {
      var date
      if (dates != '') {
        date = new Date(dates);
      } else {
        date = new Date();
      }
      var year2 = date.getFullYear()
      var index = (new Date().getFullYear() - year2) > 0 ? Math.abs(new Date().getFullYear() - year2 - 2) : 2 - (new Date().getFullYear() - year2)
      var month = date.getMonth()
      var day = date.getDate() - 1
      var hour = date.getHours()
      var minute = date.getMinutes()
      var data = [index, month, day, hour, minute]
      this.setData({
        multiIndex: data
      })
    },
  }
})