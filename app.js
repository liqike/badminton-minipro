// /app.js
import locales from './utils/locales'
import T from './utils/i18n2'


// const i18n = require('./utils/i18n')
// const en = require('./utils/language/en')
// const zh_CN = require('./utils/language/zh_CN')

// i18n.registerLocale({ en, zh_CN })

const app = getApp()

T.registerLocale(locales);	// (1)

wx.T = T; 

App({
  onLaunch: function () {
    var that=this
    if(wx.getStorageSync("token")==""){
      wx.showLoading({
        title: '授权中...',
        mask: true
      })
      wx.login({
        success: rests => {
          wx.request({
            url: that.globalData.host + '/api/v1/wxLogin/loginWithCode',
            data: {
              code: rests.code,
            },
            header: {
              "Content-Type": "application/x-www-form-urlencoded",// 默认值,
              'mobile': 't'
            },
            method: 'POST',
            success: function (res) {
              wx.hideLoading()
              if (res.data.resultCode === 20000) {
                // wx.showLoading({
                //   title: '获取用户资料...',
                //   mask: true
                // })
                wx.setStorageSync("openID", res.data.result.wxOpenid)
                wx.setStorageSync("sessionKey", res.data.result.sessionKey)
                // wx.request({
                //   url: that.globalData.host + '/api/v1/wxLogin/wxAuth',
                //   data: {
                //     openid: res.data.result.wxOpenid,
                //   },
                //   header: {
                //     'Content-type': 'application/x-www-form-urlencoded',// 默认值,
                //     'mobile': 't'
                //   },
                //   method: 'POST',
                //   success: function (res) {
                //     wx.hideLoading()
                //     if (res.data.resultCode === 20000) {
                //       wx.setStorageSync("userInfo", res.data.result.wxUsers)
                //       wx.setStorageSync("token", res.data.result.token)
                //       var realphone = res.data.result.wxUsers.userPhone
                //       console.log(realphone)
                //       var phone = realphone.substr(0, 3) + "****" + realphone.substr(7)
                //       wx.setStorageSync("realphone", realphone)
                //       wx.setStorageSync("phone", phone)
                //       wx.reLaunch({
                //         url: '/pages/index/index',
                //       })
                //     }
                //   },
                //   fail: function (res) {
                //     console.log('自动登录失败！')
                //   }
                // })
              }
            },
            fail: function (res) {
              console.log('is failed')
            }
          })
        }
      })  
    }
  

    wx.setStorageSync("mapShow", true)

    // //根据后台返回数据存储，这里写死
    wx.setStorageSync("langIndex", 0);
    T.setLocaleByIndex(wx.getStorageSync('langIndex'));	// (2)
    // i18n.setLocale(wx.getStorageSync('language') || 'zh_CN')

    wx.getSystemInfo({
      success: (res) => {
        this.globalData.height = res.statusBarHeight
      }
    })
    
    wx.setStorageSync("bg", "background-color: #1ab495;")
    wx.setStorageSync("text", "color: #fff;font-weight: bold;font-family: PingFangSC-Regular;")
  },
  globalData: {
    height:0,
    // host: 'https://www.keke520.top',
    host: 'http://localhost:9675',
  },
  verify:function(){
    var that=this
    var user = wx.getStorageSync("userInfo")
    var token=wx.getStorageSync("token")
    wx.login({
      success: rests => {
        wx.request({
          url: this.globalData.host + '/api/v1/wxLogin/verify',
          data: {
            username: user.userPhone,
            token: token
          },
          header: {
            "Content-Type": "application/x-www-form-urlencoded",// 默认值,
            'mobile': 't'
          },
          method: 'POST',
          success: function (res) {
            var code = res.data.resultCode
            if (code === 20000) {
              console.log('刷新token')
              wx.setStorageSync("token", res.data.result)
            }
          },
          fail: function (res) {
            console.log('验证失败')
          }
        })
      }
    })
  },
  refurbishToken: function () {
    var user=wx.getStorageSync("userInfo")
    wx.request({
      url: this.globalData.host + '/api/v1/wxLogin/refurbishToken',
      data: {
        userId: user.userId,
      },
      header: {
        "Content-Type": "application/x-www-form-urlencoded",// 默认值,
        'mobile': 't'
      },
      method: 'POST',
      success: function (res) {
        var code = res.data.resultCode
        if (code === 20000) {
          console.log('刷新token')
          wx.setStorageSync("token", res.data.result)
        }
      },
      fail: function (res) {
        console.log('刷新token失败')
      }
    })
  },
  refresh: function () {
    var that = this
    setInterval(that.getSession, 100*35 * 1000)
  },
  //渐入，渐出实现 
  showScale: function (that, param, scale) {
    var animation = wx.createAnimation({
      //持续时间800ms
      duration: 700,
      timingFunction: 'ease',

    });
    //var animation = this.animation
    animation.scale(scale).step().scale(1).step()
    //将param转换为key
    var json = '{"' + param + '":""}'
    json = JSON.parse(json);
    json[param] = animation.export()
    //设置动画
    that.setData(json)
  },
  //渐入，渐出实现 
  show: function (that, param, opacity) {
    var animation = wx.createAnimation({
      //持续时间800ms
      duration: 800,
      timingFunction: 'ease',

    });
    //var animation = this.animation
    animation.opacity(opacity).step()
    //将param转换为key
    var json = '{"' + param + '":""}'
    json = JSON.parse(json);
    json[param] = animation.export()
    //设置动画
    that.setData(json)
  },

  //滑动渐入渐出
  slideupshow: function (that, param, px, opacity) {
    var animation = wx.createAnimation({
      duration: 800,
      timingFunction: 'ease',
    });
    animation.translateY(px).opacity(opacity).step()
    //将param转换为key
    var json = '{"' + param + '":""}'
    json = JSON.parse(json);
    json[param] = animation.export()
    //设置动画
    that.setData(json)
  },

  //向右滑动渐入渐出
  sliderightshow: function (that, param, px, opacity) {
    var animation = wx.createAnimation({
      duration: 800,
      timingFunction: 'ease',
    });
    animation.translateX(px).opacity(opacity).step()
    //将param转换为key
    var json = '{"' + param + '":""}'
    json = JSON.parse(json);
    json[param] = animation.export()
    //设置动画
    that.setData(json)
  }
})