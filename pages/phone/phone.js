// pages/login/login.js
//获取应用实例
const app = getApp()
const login = require("../../utils/login.js");
Page({

  /**
   * 页面的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "手机登录",
    bgcolor: wx.getStorageSync("bg"),
    // loading: {
    //   type: 'circle',
    //   show: false,
    //   size: 'default',
    //   color: '#3683d6',
    //   name: 'circle'
    // }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.setStorageSync("page", "../index/index?select=1")
    /**
    * 获取系统信息
    */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getPhoneNumber: function (e) {
    // console.log(e.detail.iv);
    // console.log(e.detail.encryptedData);
    var that = this
   
    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '授权失败',
        success: function (res) { 

          wx.navigateBack({
            delta: 2
          })

        }
      })
    } else {
      wx.showLoading({
        title: '登录中...',
        mask: true
      })
      // that.setData({
      //   'loading.show':true
      // })
      wx.request({
        url: app.globalData.host + '/api/v1/wxLogin/loginWithPhone',
        data: {
          sessionKey: wx.getStorageSync("sessionKey"),
          encryptedData: e.detail.encryptedData,
          iv: e.detail.iv
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded",// 默认值
        },
        method: 'POST',
        success: function (res) {
          wx.hideLoading()
          var code = res.data.resultCode
          console.log(res)
          if (code === 20000) {
            var realphone = res.data.result.userPhone
            var phone = realphone.substr(0, 3) + "****" + realphone.substr(7)
            wx.setStorageSync("realphone", realphone)
            wx.setStorageSync("phone", phone)
            // login.auth()
            login.login(wx.getStorageSync("userInfo"))
          }
        },
        fail: function (res) {
          wx.hideLoading()
          console.log('is failed')
        }
      })
      // wx.login({
      //   success: rests => {
 
      //   }
      // })
    }  
  },

})