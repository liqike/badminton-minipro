// pages/image/imageList.js
const request = require('../../utils/requestApi')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "活动背景选择",
    winWidth: 0,
    winHeight: 0,
    height: 0,
    marginTop: 0,
    isAdd: false,
    isBack:true,
    isHeader: true,
    listImages:[],
    imageLists: [],
    imgcss: "background: hsla(0,0%,100%,.3);filter: blur(5px);",
    imgIndex:-1,
    navConfig: {
      config: {
        show: true,
        animation: "show",
        zIndex: 99,
        contentAlign: "bottom",
        locked: true,
      }
    },
    currentConf: {

    },
    activeIndex:-1,
    slideWidth: 150,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight,
          marginTop: res.statusBarHeight * 2 + 30
        });
      }
    })
    if(wx.getStorageSync("role")=='管理员'){
       that.setData({
         isAdd:true
       })
    }

    this.getImageList()
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },
  getImageList(){
    var that = this
    wx.showToast({
      title: '加载中',
      image: "../../img/loading_one.gif"
    })
    var url = "/api/v1/file"
    request.getRequest(url).then(res => {
      wx.hideToast()
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows
          console.log(res)
          that.setData({
            listImages:datas
          })
      } else {
        wx.hideToast()
       
        request.showRequestError(res)
      
      }
    }).catch(res => {
      wx.hideToast()
      request.showError()

    })
  },
  selectImage(e){
    console.log(e)
    var that = this
    var index=e.currentTarget.dataset.index
    var images = that.data.listImages 
   
    that.setData({
      imgIndex: index
    })
    setTimeout(()=>{
      let pages = getCurrentPages();
      let prevPage = pages[pages.length - 2];
      prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
        bgImage: images[index],
      })
      wx.navigateBack({
        delta: 1  // 返回上一级页面。
      })
    },100)
   
  
  },
  // 关闭滑动
  onSlideOpenTap(e) {
    const index = e.currentTarget.dataset.id
    this.setData({
      activeIndex: index
    });
  },

  addImage(){
    var that=this
    var user_id = wx.getStorageSync("userInfo").userId
    var images = this.data.imageLists
    var url = "/api/v1/file/add"
    var formData = {}
    formData.creator = user_id
    formData.fileDir= "bgImg"
    formData.fileType= 4
    wx.showToast({
      title: '添加中',
      image: "../../img/loading_one.gif"
    })
    var index=0
    for (var j in images) {
      request.uploadFile(url, images[j], formData).then(res => {
        var datas = JSON.parse(res.data)
        console.log(datas)
        if (datas.resultCode == 20000) {
           if(index==j){
             console.log(index)
              wx.hideToast()
              that.onHidePopupTap()
              that.getImageList()
           }
        } else {
          wx.hideToast()
          request.showRequestError(res)
        } 
        index++
      }).catch(res => {
        wx.hideToast()
        request.showError()
      })
    }
  },
  delBgimg(e){
    var that = this
    if (wx.getStorageSync('role') != '管理员') {
      wx.showToast({
        title: '您不是管理员不能删除!',
        icon: 'none',
      })
    } else {
    var id=e.currentTarget.dataset.id
    const index = e.currentTarget.dataset.index
    that.setData({
      activeIndex: -1
    })
    var url = "/api/v1/file/"
    request.deleteRequest(url, id).then(res => {
      if (res.data.resultCode == 20000) {
        var lists = that.data.listImages
        lists.splice(index, 1)
        console.log(lists)
        if (lists.length == 0) {
          that.getImageList()
        }
        that.setData({
          listImages: lists
        })
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
    }
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  // 显示Popup
  onShowPopupTap() {
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
    this.setData({
      currentConf: config,
      type: 2,
      name: "",
      address: "",
      isBack: false,
      isAdd: false,
      title: "添加背景照片"
    })
  },
  // 隐藏Popup
  onHidePopupTap() {
    this.data.currentConf.show = false
    this.setData({
      currentConf: this.data.currentConf,
      isBack: true,
      isAdd: true,
      title: "活动背景选择"
    })
  },
  //照片操作
  onClearTap(e) {
    console.log("shanchu",e)
  },
  onChangeTap(e) {
    const count = e.detail.current.length
    this.setData({
      imageLists: e.detail.all
    })
  },
  onRemoveTap(e) {
    const index = e.detail.index
    var imagelists=this.data.imageLists
    imagelists.splice(index, 1)
    this.setData({
      imageLists: imagelists
    })

  },
})