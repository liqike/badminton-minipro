//index.js
//获取应用实例
const app = getApp()
const login = require("../../utils/login.js");
const map = require("../../utils/map.js");
import timeHandle from '../../utils/timeHandle';
const request = require('../../utils/requestApi')
const util = require('../../utils/util')
const echars = require("../../utils/echars.js");
import * as echarts from '../../ec-canvas/echarts'
const pageNum = 1//页码
const pageSize = 4 //每页的查询数据
var barec1 = null
var barec2 = null
var barec3 = null
// const CHARTS = require('../../echars/wxcharts-min.js'); // 引入wx-charts.js文件
// function initChart(canvas, width, height, data) {
//   const chart = echarts.init(canvas, null, {
//     width: width,
//     height: height
//   });
//   canvas.setChart(chart);

//   chart.setOption(option);
//   return chart;
// }

Page({
  data: {

    ec1: {
      onInit: function (canvas, width, height, data) {
        barec1 = echarts.init(canvas, null, {
          width: width,
          height: height
        });
        canvas.setChart(barec1);
        return barec1;
      }
    },
    ec2: {
      onInit: function (canvas, width, height, data) {
        barec2 = echarts.init(canvas, null, {
          width: width,
          height: height
        });
        canvas.setChart(barec2);
        return barec2;
      }
    },
    ec3: {
      onInit: function (canvas, width, height, data) {
        barec3 = echarts.init(canvas, null, {
          width: width,
          height: height
        });
        canvas.setChart(barec3);
        return barec3;
      }
    },

    currentTab: 1,
    winHeight: 0,
    winWidth:0,

    lineshow: false,
    login:false,
    city: "定位中...",
    address: "",


    config4: {
      show: true,
      selected: 1,
      color: '#18ce82',
      selectedColor: "#18ce82",
      borderStyle: "#f6f6f6",
      backgroundColor: "#fff",
      fontSize: 22,
      list: [
        {
      // pagePath: "../user/user",
      iconPath: '../../img/bad-foot-my.png',
      iconSize: 70,
      selectedIconPath: '../../img/bad-foot-all.png',
      text: "我的",
      entext: "Me"
    },
    //  {
    //     // pagePath: "../club/index",
    //       iconPath: '../../img/bad-club-1.png',
    //     selectedIconPath: '../../img/bad-foot-all.png',
    //     iconSize: 70,
    //     text: "俱乐部",
    //       entext: "Club"
    //   }, 
      {
        // pagePath: "../index/index",
        iconSize: 70,
        style: 'circle',
          iconPath: '../../img/bad-top.png',
          selectedIconPath: '../../img/bad-top.png',
          text: "生涯",
          entext: "Top"
      }, 
      // {
      //   // pagePath: "../games/gameList",
      //     iconPath: '../../img/bad-tour-2.png',
      //   iconSize: 70,
      //   // badge: 99,
      //   selectedIconPath: '../../img/bad-foot-all.png',
      //   text: "比赛",
      //     entext: "Games"
      // },
       {
        // pagePath: "../others/index",
        iconPath: '../../img/bad-foot-other.png',
        iconSize: 70,
        // redDot: true,
        selectedIconPath: '../../img/bad-foot-all.png',
        text: "其它",
        entext: "Other"
      }
      ]
    },

   activeIndex:-1,
    scrollTop: 0,

    gameLists: [],
    gameTimes: 0,
    gamePrice: 0,
    homeScore: 0,
    awayScore: 0,


    totalTimesS:0,
    totalTimesD:0,
    winTimesS:0,
    winTimesD:0,

    delId: "",
    delIndex: -1,

    currentDate:"",
    dateIscheck:false,
    is_update_data:true,
    update_data:{},
    update_index:-1,

    pageNum : 1,//页码
    pageSize : 3, //每页的查询数据
    totalPage:0, //共计页数


    config: [{
      type: 'loading',
      custom: false,
      line: true,
      color: '',
      loadingText: '加载中...',
      endText: '我是有底线的~'
    }
    ],



    loading: {
      type: 'circle',
      show: false,
      size: 'default',
      color: '#34bfa3',
      name: 'circle'
    },
    loading3: {
      type: 'circle',
      show: false,
      size: 'default',
      color: '#34bfa3',
      name: 'circle'
    },
    loadingend:false,
    imgUrls: [
'https://www.keke520.top/img/bgImg/d410dcffe5dd45b88290a73c735a8308624737454993178625.jpg',
'https://www.keke520.top/img/bgImg/f691798c47f14823ad8dc0055a2c3c4e624737455068676097.jpg',
'https://www.keke520.top/img/bgImg/35cd6b66b5c642439254b238db621305624737455127396353.jpg',

    ],
    show:false,
    tip:false,
    tip_title:"",
    login_title:"快来开启您的生涯",

    navConfig: {
      title: "退出系统",
      type: 1,
      config: {
        show: true,
        type: "confirm",
        showTitle: true,
        title: "删除活动",
        content: "确认删除活动吗？",
        confirmText: '确定',
        confirmColor: '#3683d6',
        cancelText: '取消',
        cancelColor: '#999'
      }
    },
    currentConf2: {

    },

    selected: [],

    monthDate:0,
    yearDate:0,
    isIphoneX:false,
    isScrolling: false,
    badTimes:[0,0,0,0,0,0,0,0,0,0,0,0],

  },
  onLoad: function (options) {
    var that=this
    wx.getSystemInfo({
      success: function (res) {
        let modelmes = res.model;
        if (modelmes.search('iPhone X') != -1) {
          that.setData({
            isIphoneX: true,
          });
        }
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight,
          currentDate:util.formatTime2(new Date()),
          monthDate: new Date().getMonth() + 1,
          yearDate: new Date().getFullYear(),
        });
      }
    })
    var selectIndex = options.select == null ? 1 : options.select
    this.setData({
      currentTab: selectIndex
    })
   
    map.getUserLocation().then(data => {
      var options = data[0]
      this.setData({
        city: options.city,
        address: options.address
      })
    })
   
    if (wx.getStorageSync("openID") != "" && wx.getStorageSync("token")!=""){
      // this.listGames()
    
      this.setData({
        login:true
      })
     
    // if (selectIndex == 1) {
    //   this.listGames()
    // }
    }
   
   
  },
  changeTabs(e) {
    console.log(e)
  },

  onShow() {
    wx.setStorageSync("page", "../index/index?select=1")
    if (this.data.currentTab==-1){
      var e={}
      var detail={}
      detail.index=0
      e.detail=detail
      this.okEvent(e)
    }
  
    if (wx.getStorageSync("openID") != "" && wx.getStorageSync("token") != "") {
      if (this.data.currentTab == 1) {
        if (this.data.update_data.id != null){
          var gamesList=this.data.gameLists
          gamesList[this.data.update_index]=this.data.update_data
          this.setData({
            gameLists: gamesList
          })
        }
        this.listGames(this.data.currentDate, this.data.pageNum) 
        this.getdakaDate(this.data.currentDate)     
      }
    }
   
  },
  user_login(){
    if (wx.getStorageSync("userInfo")=='') {
      wx.navigateTo({
        url: '/pages/login/login',
      })
    } else {
      login.getSessionKey()
    }
    
  },
  // 查看是否授权
  bindGetUserInfo: function (e) {
    var that = this
    wx.setStorageSync("userInfo", e.detail.userInfo);
    if (e.detail.errMsg == 'getUserInfo:fail auth deny') {
      wx.showModal({
        showCancel: false,
        content: '未授权',
        success: function (res) {
          wx.navigateBack({
            url: wx.getStorageSync("page")
          })
          wx.hideLoading()
        }
      })
    } else {
      login.getSessionKey()
    }

  },
  okEvent:function(e){
    let that = this;
    if (this.data.currentTab === e.detail.index) {
      return false;
    } else {
      if (wx.getStorageSync("openID") != "" && wx.getStorageSync("token") != "") {
        if (e.detail.index== 1) {
          this.listGames(that.data.currentDate, that.data.pageNum)
          this.getdakaDate(that.data.currentDate)
        }
        if (e.detail.index == 0){
          var year = new Date(that.data.currentDate.replace(/-/g, "/")).getFullYear()
          this.getYearDate(year)
          this.getCountTimes()
        }
      }
      that.setData({
        currentTab: e.detail.index
      })
    }
  },
  scroll: function (event) {
    this.setData({
      scrollTop: event.detail.scrollTop
    });
  },
  // 下拉刷新
  refresh: function (e) {
    var that = this;
    // console.log(that.data.scrollTop)
      if (that.data.scrollTop < -10) {
        that.setData({
          pageNum: 1,
          loading: { show: true },
          isScrolling: false,
          config: [{
            type: 'loading',
            custom: false,
            line: true,
            color: '',
            loadingText: '加载中...',
            endText: '我是有底线的~'
          }]
        })
        setTimeout(function () {
          that.listGames(that.data.currentDate,1)
        }, 500);
    }
  },
  //上拉
  getOthers: function (e) {
    var that = this
    if (this.data.isScrolling === true)
      return;
 
    that.setData({
      isScrolling: true,
      loadingend: true,
      is_update_data:true
    })
    if (that.data.pageNum == that.data.totalPage) {
      that.setData({
        config: [{
          type: 'end',
          custom: false,
          is_update_data: false,
          line: true,
          color: '',
          loadingText: '加载中...',
          endText: '我是有底线的~'
        }]
      })
    } else {
        that.setData({
           pageNum:that.data.pageNum + 1,
        })
        setTimeout(()=>{
          that.listGames(that.data.currentDate, that.data.pageNum)
        },300)    
    }
   
  },
  //生涯列表
  listGames(now,pageNum) {
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/standbylist/gameList"
    var data={}
    // data.isSignup = 1
    data.userId = user_id
    data.pageSize = that.data.pageSize
    data.pageNum = pageNum
    
    var datetime = that.getAppointedDate(now,2, 0) 
    // console.log(pageNum)
    data.startDate = datetime.startDate
    data.endDate = datetime.endDate
    if (pageNum == 1) {
      that.setData({
        loading: { show: true },
      })
    }
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows
        var size=that.data.pageSize
        var totalNum = res.data.result.total
        var totalPage = totalNum % size == 0 ? parseInt(totalNum / size) : parseInt(totalNum / size) + 1

        datas.forEach(item => {
          item.tournaPersonal.evtSTime = timeHandle.timeHandle(new Date(item.tournaPersonal.evtSTime.replace(/-/g, "/")))
          item.tournaPersonal.createDate = timeHandle.timeHandle(new Date(item.tournaPersonal.createDate.replace(/-/g, "/")))
        });
        var title=""
        if (that.data.monthDate > (new Date().getMonth() + 1) || that.data.yearDate > (new Date().getFullYear())){
          title = "亲," + that.data.monthDate + "月还没有到来哦!\n可以提前预约羽友!"
        }else{
          title = "陛下," + that.data.monthDate + "月居然还未打过一场羽毛球？！"
        }
        console.log(datas)
        if (totalNum == 0) {
          that.setData({
            tip: true,
            tip_title: title,
            lineshow: false,
            loadingend: false,
            login:true,
            gameLists: datas,
            loading: { show: false }
          })
        } else if (totalNum <= size){
          that.setData({
            totalPage: totalPage,
            gameLists: datas,
            login: true,
            tip: false,
            pageNum: 1,
            loading: { show: false },
          })
        }else{
          if(pageNum==1){
            that.setData({
              totalPage: totalPage,
              gameLists: datas,
              tip: false,
              isScrolling: false,
              loadingend: false,
              login: true,
              pageNum: 1,
              loading: { show: false },
              config: [{
                type: 'loading',
                custom: false,
                line: true,
                color: '',
                loadingText: '加载中...',
                endText: '我是有底线的~'
              }]
            })
          }

          if (pageNum>1 && pageNum <= totalPage) {
            if (that.data.is_update_data) {
              var tempArray = that.data.gameLists;
              tempArray = tempArray.concat(datas);
              console.log(tempArray)
              that.setData({
                totalPage: totalPage,
                gameLists: tempArray,
                isScrolling:false,
                loadingend: false,
                login: true,
                loading: { show: false }
              })
            }
          } 

        }
      } else {
        // wx.hideToast()
        request.showRequestError(res)
      }
    }).catch(res => {
      // wx.hideToast()
      request.showError()
    })
  },
  /**
   * 获取打卡记录
   */
  getdakaDate(now) {
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/standbylist/gameList"
    var data = {}
    // data.isSignup = 1
    data.userId = user_id
    data.pageSize = 200
    data.pageNum = 1
    var datetime = that.getAppointedDate(now,2, 0)
    data.startDate = datetime.startDate
    data.endDate = datetime.endDate

    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows

          var selected = []
        for (var data in datas) {
          var time = {}
          time.date = util.formatTime2(new Date(datas[data].tournaPersonal.evtSTime.replace(/-/g, "/")))
          selected.push(time)
        }
        var totalPrice = 0
        datas.forEach(item => {
          totalPrice = totalPrice + (item.fee == null ? 0 : item.fee)
        });
        that.setData({
          gameTimes: datas.length,
          gamePrice: totalPrice,
          selected: selected,
          // dateIscheck: false,
          // currentDate: now
        })
        
      } else {
        // wx.hideToast()
        request.showRequestError(res)
      }
    }).catch(res => {
      // wx.hideToast()
      request.showError()
    })
  },
  /**
   * 数据统计年度打卡次数
   */
  getYearDate(year) {
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/standbylist/gamelistYear"
    var data = {}
    data.userId = user_id
    data.year = year
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result
        var nums = that.data.badTimes
        for (var i in datas){
          nums[datas[i].month-1] = datas[i].times
        }
        var option = {
          tooltip: {
            trigger: 'axis',
            position: 'top',
            formatter: function (params) {
              return '2019年' + params[0].axisValue + '月' + '\n' + '打卡:' + params[0].value + '次';
            },
            axisPointer: {
              type: 'none',
              lineStyle: {
                color: '#ddd'
              }
            },
            backgroundColor: 'black',
            padding: [2, 4],
            textStyle: {
              color: 'white',
            },
            extraCssText: 'box-shadow: 0 0 5px rgba(0,0,0,0.3)'
          },
          xAxis: {
            type: 'category',
            name: '',
            data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'],
            boundaryGap: false,
            splitLine: {
              show: false,
              interval: 'auto',
              lineStyle: {
                color: ['#EEEEEE']
              }
            },
            axisTick: {
              show: false
            },
            axisLine: {
              lineStyle: {
                color: '#50697A'
              }
            },
            axisLabel: {
              margin: 10,
              textStyle: {
                color: 'black',
                fontSize: 12
              }
            }
          },
          yAxis: {
            type: 'value',
            // name: '次',
            splitLine: {
              lineStyle: {
                color: ['#EEEEEE']
              }
            },
            axisTick: {
              show: false
            },
            axisLine: {
              show: false
            },
            axisLabel: {
              margin: 5,
              textStyle: {
                color: 'black',
                fontSize: 10
              }
            }
          },
          series: [{
            name: '打卡',
            type: 'line',
            smooth: true,
            showSymbol: false,
            symbol: 'circle',
            symbolSize: 6,
            data: nums,//['32', '14', '0', '0', '23', '23', '45', '23', '3', '12', '3', '22']
            areaStyle: {
              normal: {
                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                  offset: 0,
                  color: 'rgba(10, 138, 21, 0.932)'
                }, {
                  offset: 1,
                  color: 'rgba(10, 138, 21, 0.332)'
                }], false)
              }
            },
            itemStyle: {
              normal: {
                color: 'black'
              }
            },
            lineStyle: {
              normal: {
                width: 0.5
              }
            }
          }]
        };

        that.setData({
          badTimes: nums
        })

        setTimeout(()=>{
          barec2.setOption(option);
          setTimeout(() => {
          barec2.dispatchAction({
            type: 'showTip',
            seriesIndex: 0,  // 显示第几个series
            dataIndex: new Date().getMonth()// 显示第几个数据
          })
          }, 1000)
        },300)
      
       
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  /**
 * 数据统计 单双打胜率
 */
  getCountTimes() {
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/standbylist/gamelistCount"
    var data = {}
    data.userId = user_id
    that.setData({
      loading3: { show: true }
    })
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result
        var dwin =0
        var swin=0
        if (datas[1].totalTimes != 0) {
          dwin = (datas[1].winTimes / datas[1].totalTimes * 100).toFixed(1)
        }
        if (datas[0].totalTimes!=0){
          swin = (datas[0].winTimes / datas[0].totalTimes * 100).toFixed(1)
        }
       
        setTimeout(()=>{
          barec1.setOption(echars.getPeih(swin));
          barec3.setOption(echars.getPeih(dwin));
          that.setData({
            totalTimesS: datas[0].totalTimes,
            totalTimesD: datas[1].totalTimes,
            loading3: { show: false }
          })
        },300)

      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  sharetap(e){
    var game= e.detail.game
    var that=this
    wx.navigateTo({
      url: '../games/share/share?listId=' + game.id + "&evtId=" + game.evtId+ "&is_btn=" + 2,
    })
    setTimeout(function () {
      that.setData({
        activeIndex: -1
      });
    }, 1000)
   
  },
  lookGameList(e){
    var listId = e.detail.listId
    var evtId = e.detail.evtId
    var name = e.detail.name
    var index = e.detail.index
    var isSignup = e.detail.isSignup
    this.setData({
      update_index:index
    })
    wx.navigateTo({ 
      url: '../games/list/gameList?listId=' + listId + "&evtId=" + evtId + "&name=" + name + "&isSignup=" + isSignup + "&index=" + index,
    })
  },
  del(e) {
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
    this.setData({
      currentConf2: config
    })
    var id = e.detail.id
    var index = e.detail.index
    this.setData({
      delId:id,
      delIndex:index
    })
  },
  onConfirmTap(e) {
    this.delGame()
  },
  delGame(){
    var that = this
    var id = that.data.delId
    var index = that.data.delIndex
    that.setData({
      activeIndex: -1
    })
    var data={}
    data.evtId=id
    var url = "/api/v1/standbylist/batch"
    request.putRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var lists = that.data.gameLists
        lists.splice(index,1)
        if (lists.length==0){
          that.listGames(1)
        }
        that.setData({
          gameLists: lists
        })
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },

  /**
 * 用户点击右上角分享
 */
  onShareAppMessage: function () {

  },
  onReady:function(){
    this.list = this.selectComponent("#list");
  },

  /**
     * 页面上拉触底事件的处理函数
     */
  onReachBottom: function () {
    let that = this;
   
  },
 /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    // if (this.data.currentTab==1){
    //   wx.showNavigationBarLoading()
    //   this.listGames()
    //   wx.hideNavigationBarLoading()
    //   wx.stopPullDownRefresh()
    // }
},
  addGameInfo(e) {
    wx.vibrateShort();
    wx.navigateTo({
      url: '../games/add/addGameInfo',
    })
  }, 
  /**
 * 日历是否被打开
 */
  bindselect(e) {
    this.setData({
      dateIscheck: e.detail.ischeck
    })
  },
  /**
   * 获取选择日期
   */
  bindgetdate(e) {
    let time = e.detail;
    var currentTime=time.year+"-"+time.month+"-"+time.date
    if (wx.getStorageSync("openID") != "" && wx.getStorageSync("token") != "") {
      if (this.data.dateIscheck && this.data.currentDate != currentTime) {
        this.setData({
          currentDate: currentTime,
          monthDate: time.month,
          yearDate: time.year,
        })
        this.getdakaDate(currentTime)
        this.listGames(currentTime, 1)
      }
    }
    

  },
  getAppointedDate(currentDate,optType, optPageNum) {
        let now = new Date(currentDate.replace(/-/g, "/")), //当前日期
      　　nowDayOfWeek = now.getDay(), //今天本周的第几天
      　　nowDay = now.getDate(), //当前日
      　　nowMonth = now.getMonth(), //当前月
      　　nowYear = now.getFullYear(), //当前年
      　　lastMonth = nowMonth - 1;
    　　let startDate = '', endDate = '';
    　　if (optType == 1) {
      　　　　startDate = this.formatDateFn(new Date(nowYear, nowMonth, nowDay - nowDayOfWeek + 1 - (7 * optPageNum)))
      　　　　endDate = this.formatDateFn(new Date(nowYear, nowMonth, nowDay + 7 - nowDayOfWeek - (7 * optPageNum)))
    　　} else if (optType == 2) {
      　　　　startDate = this.formatDateFn(new Date(nowYear, lastMonth + 1 - (1 * optPageNum), 1))
      　　　　endDate = this.formatDateFn(new Date(nowYear, lastMonth + 1 - (1 * optPageNum), this.getMonthDays(nowYear, lastMonth + 1 - (1 * optPageNum))))
    　　}
    　　return { startDate: startDate, endDate: endDate }
  },
  getMonthDays(nowYear, myMonth) {
    　　let monthStartDate = new Date(nowYear, myMonth, 1);
    　　let monthEndDate = new Date(nowYear, myMonth + 1, 1);
    　　let days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24);
    　　return days;
  },
  formatDateFn(date) {
    　　let myyear = date.getFullYear();
    　　let mymonth = date.getMonth() + 1;
    　　let myweekday = date.getDate();
    　　if (mymonth < 10) {
      　　　　mymonth = "0" + mymonth;
    　　}
    　　if (myweekday < 10) {
      　　myweekday = "0" + myweekday;
    　　}
    　　return (myyear + "-" + mymonth + "-" + myweekday);

  },

})
