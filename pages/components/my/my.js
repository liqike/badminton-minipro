// pages/components/my.js
const login = require("../../../utils/login.js");
const util = require('../../../utils/util');
import event from '../../../utils/event'
const regeneratorRuntime = require("../../../regenerator/runtime");
const request = require('../../../utils/requestApi')
const app = getApp()


Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },
  // 使用 Object.assign 补充翻译的 data
  data: {
    windowWidth: wx.getSystemInfoSync().windowWidth,
    windowHeight: wx.getSystemInfoSync().windowHeight,
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "用户信息",
    entitle:"UserInfo",
    // bgcolor: wx.getStorageSync("bg"),
    // text: wx.getStorageSync("text"),
    share: false,
    userInfo: {
      wxImg: "",//用户头像  
      wxNickname: "",//用户昵称 
      wxCity: "",
      wxCountry: "",
      wxProvince: "",
      wxGender: "",
      language: "",
    },
    flag: '2',
    phone: '',
    roleName: '',
    bind: false,
    logout: false,
    loading: false,


    config: {
      show: true,
      title: "先行告退！",
      icon: "shangxin",
      iconStyle: "size:60; color:#00C292",
      image: "",
      imageStyle: "",
      placement: '',
      duration: 1500,
      center: false,
      mask: false
    },
    currentConf:{

    },

    navConfig: {
      title: "退出系统",
      type: 1,
      config: {
        show: true,
        type: "confirm",
        showTitle: true,
        title: "退出小程序",
        content: "确认退出小程序吗？",
        confirmText: '确定',
        confirmColor: '#3683d6',
        cancelText: '取消',
        cancelColor: '#999'
      }
    },
    currentConf2: {

    },
  },
lifetimes: {
 
  attached() {
    var that = this
    wx.setStorageSync("page", "../index/index?select=0")

    // console.log(1111111)
    // // 代理小程序的 setData，在更新数据后，翻译传参类型的字符串
    // util.resetSetData.call(this, langData)
    //来确保用户退出之后重新进入小程序时仍能正常显示当前使用的语言。
    this.setLanguage();	// (1)
    event.on("languageChanged", this, this.setLanguage); // (2)

    /**
    * 获取系统信息
    */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight
        });
      }
    })

    var that = this
    if (wx.getStorageSync("userInfo") == '') {
      that.setData({
        bind: true
      })
    } else {
      if (wx.getStorageSync("phone") == '') {
        this.setData({
          bind: true,
          logout: false
        })
      } else {
        that.setData({
          userInfo: wx.getStorageSync("userInfo"),
          phone: wx.getStorageSync("phone"),
          roleName: wx.getStorageSync("roleName"),
          bind: false,
          logout: true
        })
      }
    }

    app.show(this, 'slide_up1',  1)

    setTimeout(function () {
      app.show(this, 'slide_up2',  1)
    }.bind(this), 200);

    setTimeout(function () {
      app.show(this, 'slide_up3',  1)
    }.bind(this), 400);

    
   },
  detached: function () {
    // 在组件实例被从页面节点树移除时执行
    //你可以看到，动画参数的200,0与渐入时的-200,1刚好是相反的，其实也就做到了页面还原的作用，使页面重新打开时重新展示动画
    app.show(this, 'slide_up1', 0)
    //延时展现容器2，做到瀑布流的效果，见上面预览图
    setTimeout(function () {
      app.show(this, 'slide_up2',  0)
    }.bind(this), 200);
    setTimeout(function () {
      app.show(this, 'slide_up3',  0)
    }.bind(this), 400);
  },

  },
  /**
   * 组件的方法列表
   */
  methods: {
    // 查看是否授权
    bindGetUserInfo: function (e) {
      var that = this
      wx.setStorageSync("userInfo", e.detail.userInfo);
      if (e.detail.errMsg == 'getUserInfo:fail auth deny') {
        wx.showModal({
          showCancel: false,
          content: '未授权',
          success: function (res) {
            wx.navigateBack({
              url: wx.getStorageSync("page")
            })
            wx.hideLoading()
          }
        })
      } else {
        login.getSessionKey()
      }

    },
    setLanguage() {
      this.setData({
        language: wx.T.getLanguage()
      });
    },
    islogout(){
      const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
      this.setData({
        currentConf2: config
      })
    },
    onConfirmTap(e) {
      this.logout()
    },
    logout: function () {
      var that = this
      var url = "/api/v1/wxLogin/logout"
      var token=wx.getStorageSync("token")
      var data={}
      data.token = token
      request.getRequest(url,data).then(res => {    //！！！注意括号的个数！！！
        that.setData({
          loading: true,
        })
        setTimeout(() => {
          wx.removeStorageSync("phone")
          wx.removeStorageSync("sessionKey")
          wx.removeStorageSync("openID")
          wx.removeStorageSync("realphone")
          wx.removeStorageSync("roleName")
          wx.removeStorageSync("token")


          that.setData({
            bind: true,
            logout: false,
            userInfo: {
              wxNickname: '',
              wxImg: '',
              roleName: ''
            }
          }, function () {
            const config = JSON.parse(JSON.stringify(this.data.config))
            this.setData({
              currentConf: config
            })
          })
        }, 1000)
      }).catch(res => {
        request.showError()
      })
       
        
     

    },
    editUser:function(){
      wx.navigateTo({
        url: '../user/user',
      })
    },
    onPopupTap() {
      const type = this.data.type
      if (type === 6) {
        wx.showToast({
          title: '请点击按钮取消！',
          icon: 'none'
        })
      }

    },
    onValueChange(e) {
      this.setData({ value: e.detail.value })
      console.log('onValueChange', e.detail)
    },

    // 显示Popup
    onShowPopupTap(e) {
      const type = e.currentTarget.dataset.type
      const config = JSON.parse(JSON.stringify(this.data.config))
      this.setData({
        currentConf: config
      })
    },

    // 隐藏Popup
    onHidePopupTap() {
      const type = this.data.type
      this.data.currentConf.show = false
      this.setData({
        currentConf: this.data.currentConf
      })

      if (type === 3) {
        wx.showToast({
          title: '已取消~',
          icon: 'none'
        })
      }
    },
  }
})
