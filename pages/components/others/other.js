// pages/components/others/other.js
import event from '../../../utils/event'
const app = getApp()
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "其它",
    entitle: "Others",

    language: {},
    languages: ["简体中文", "English"],





  },
  attached() {
    var that = this
    wx.setStorageSync("page", "../index/index?select=2")
    /**
     * 获取系统信息
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight
        });
      }
    })
    //来确保用户退出之后重新进入小程序时仍能正常显示当前使用的语言。
    that.setLanguage();	// (1)
    event.on("languageChanged", that, that.setLanguage); // (2)
  },
  /**
   * 组件的方法列表
   */
  methods: {
    setLanguage() {
      this.setData({
        language: wx.T.getLanguage()
      });
    },
  }
})
