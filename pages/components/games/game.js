// pages/components/games/game.js
Component({
  options: {
    addGlobalClass: true,
  },
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "比赛",
    entitle: "Games",
    swiperCurrent:0,
    imgUrls: [
      '../../../img/bg3.jpg',
      '../../../img/bg3.jpg',
      '../../../img/bg3.jpg'
    ],
  },
  attached() {
    var that = this
    /**
     * 获取系统信息
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight
        });
      }
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    //轮播图的切换事件
    swiperChange: function (e) {
     this.setData({
        swiperCurrent: e.detail.current
      })
    },
    //点击图片触发事件
    swipclick: function (e) {
      // console.log(this.data.swiperCurrent);
      // wx.switchTab({
      //   url: this.data.links[this.data.swiperCurrent]
      // })
    },
 
  }
})
