// pages/components/club/club.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {

  },

  /**
   * 组件的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "俱乐部",

    navConfig: [],
    currentConf: {
      title: "从下弹出案例",
      type: 4,
      config: {
        show: true,
        animation: "show",
        zIndex: 99,
        contentAlign: "bottom",
        locked: false,
      }
    }
     
  },
  attached() {
    var that = this
    /**
     * 获取系统信息
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight
        });
      }
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {

  }

})
