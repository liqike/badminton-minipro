// pages/components/index/index.js
import  timeHandle  from '../../../utils/timeHandle';
const regeneratorRuntime = require("../../../regenerator/runtime");
const request = require('../../../utils/requestApi')
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    gameLists: {
      type: Object,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: [],
    },
    gameTimes: {
      type: Number,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: 0,
    },
    gamePrice: {
      type: Number,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: 0,
    },
    homeScore: {
      type: Number,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: 0,
    },
    awayScore: {
      type: Number,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: 0,
    },
    activeIndex: {
      type: Number,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: -1,
    },
  },

  /**
   * 组件的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "",
    isBack: false,
    component:{},



    alreadyLoadData: false,
    posting:false,
    pageIndex: 1,
    pageCount: 1,
    topInfo:[1],

    imgShow: false,
    show: true,
    loading: true,
    index: 0,

    newsList: [],
    slideWidth: 300,
  },
  pageLifetimes: {
    show() {
      var that = this
      
    }
  },
  attached() {
    var that = this
    /**
    * 获取系统信息
    */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight ,
          height: res.statusBarHeight,
          show: true
        });
      }
    })
  },
  /**
   * 组件的方法列表
   */
  methods: {
    lookGameList(e){
      var listId=e.currentTarget.dataset.id
      var evtId = e.currentTarget.dataset.evtid
      var name = e.currentTarget.dataset.name
      var isSignup = e.currentTarget.dataset.isjoin
      var index = e.currentTarget.dataset.index
      // wx.navigateTo({
      //   url: '../games/list/gameList?id='+id,
      // })
      this.triggerEvent('lookGameList', { listId, evtId, name, isSignup, index}, {})
    },
    onSlideOpenTap(e) {
      wx.vibrateShort();
      const index= e.currentTarget.dataset.id
      this.setData({
        activeIndex: index
      });
    },
    lookGameInfo(e){
      var id=e.currentTarget.dataset.id
      wx.navigateTo({
        url: '../games/add/addGameInfo?id=' + id,
      })
    },
    //删除生涯
    delGame(e){
      var id = e.currentTarget.dataset.id
      var index = e.currentTarget.dataset.index
      this.triggerEvent('del', { id, index}, {})
    },
    onLazyLoad(info) {
      console.log(info)
    },
    sharetap(e){
      var that=this
      var game = e.currentTarget.dataset.game

      setTimeout(function(){
        that.setData({
          activeIndex: -1
        });
      },230)
      
      this.triggerEvent('sharetap', {game}, {})
    }
  }
})
