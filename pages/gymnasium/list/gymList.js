// pages/gymnasium/gymList.js
const util = require('../../../utils/util')
const request = require('../../../utils/requestApi')
const map = require('../../../utils/map')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "选择场馆",
    winWidth: 0,
    winHeight: 0,
    height: 0,
    activeIndex:-1,
    isBack:true,
    isAdd:true,
    disabled:true,
    name: "",
    address: "",
    latitude:0,
    longitude:0,
    currentLatitude:0,
    currentLongitude: 0,
    navConfig: {
      config: {
        show: true,
        animation: "show",
        zIndex: 99,
        contentAlign: "bottom",
        locked: true,
      }
    },
    currentConf: {
     
    },
    listGyms:[],
    gym:{},
    slideWidth: 140,

    loading: {
      type: 'circle',
      show: true,
      size: 'default',
      color: '#34bfa3',
      name: 'circle'
    },


    isScrolling: false,
    loadingend: false,
    scrollTop: 0,
    pageNum: 1,//页码
    pageSize: 10, //每页的查询数据
    totalPage: 0, //共计页数
    config: [{
      type: 'loading',
      custom: false,
      line: true,
      color: '',
      loadingText: '加载中...',
      endText: '我是有底线的~'
    }
    ],
    loading2: {
      type: 'circle',
      show: false,
      size: 'default',
      color: 'black',
      name: 'circle'
    },

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    /**
      * 获取系统信息
      */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight,
          show: true
        });
      }
    })
    map.getUserLocation().then(data => {
      var options = data[0]
      this.setData({
        currentLatitude: options.latitude,
        currentLongitude: options.longitude
      })
      this.listGym(1)
    })
  },
  onChange(e) {
    console.log(e)
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  // 显示Popup
  onShowPopupTap() {
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
    this.setData({
      currentConf: config,
      type:2,
      name: "",
      disabled:"disabled",
      address: "",
      isBack:false,
      isAdd:false,
      title:"添加场馆"
    })
  },

  onHidePopupTap() {
    this.data.currentConf.show = false
    this.setData({
      currentConf: this.data.currentConf,
      isBack: true,
      isAdd: true,
      title: "场馆"
    })
  },
  del(e){
   if(wx.getStorageSync('role')!='管理员'){
     wx.showToast({
       title: '您不是管理员不能删除!',
       icon: 'none',
     })
   }else{
   var that=this
    wx.showToast({
      title: '删除中',
      image: "../../../img/loading_one.gif"
    })
   var id=e.currentTarget.dataset.id
    var index = e.currentTarget.dataset.index
    var url = "/api/v1/stadium/"
    request.deleteRequest(url, id).then(res => {
      if (res.data.resultCode == 20000) {
        var lists = that.data.listGyms
        lists.splice(index, 1)
        if (lists.length == 0) {
          that.listGym(1)
        }
        that.setData({
          listGyms: lists,
          activeIndex:-1
        })
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
   }
  },
  confrim(){
    var gym = this.data.gym
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2]; 
    prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
      gym: gym,
    })
    wx.navigateBack({
      delta: 1  // 返回上一级页面。
    })

  },
  checkRadio(e){
    var gym_id=e.currentTarget.dataset.id
    var datas=this.data.listGyms
    var data={}
    for (var i = 0; i < datas.length; i++) {
      if(datas[i].id==gym_id){
        datas[i].checked = true
        data = datas[i]
      }else{
        datas[i].checked = false
      }
    }
    this.setData({
      listGyms: datas,
      gym: data
    })
  },
  chooseLocation(){
    var that = this
    wx.chooseLocation({
      success: function (res) {
        var address = res.address
        var name = res.name
        var longitude = res.longitude
        var latitude = res.latitude
        that.setData({
          name:name,
          address: address,
          latitude: latitude,
          longitude: longitude,
          disabled:false
        })
      }
    })
  },
  scroll: function (event) {
    this.setData({
      scrollTop: event.detail.scrollTop
    });
  },
  // 下拉刷新
  refresh: function (e) {
    var that = this;
    // console.log(that.data.scrollTop)
    if (that.data.scrollTop < -10) {
      that.setData({
        pageNum: 1,
        loading2: { show: true },
        isScrolling: false,
      })
      setTimeout(function () {
        that.listGym(1)
      }, 500);
    }
  },
  //上拉
  getOthers: function (e) {
    var that = this
    if (this.data.isScrolling === true)
      return;
    that.setData({
      isScrolling: true,
      loadingend: true,
    })
    if (that.data.pageNum == that.data.totalPage) {
      that.setData({
        config: [{
          type: 'end',
          custom: false,
          line: true,
          color: '',
          loadingText: '加载中...',
          endText: '我是有底线的~'
        }]
      })
    } else {
      that.setData({
        pageNum: that.data.pageNum + 1,
      })
      setTimeout(() => {
        that.listGym(that.data.pageNum)
      }, 300)
    }

  },
  listGym(pageNum){
    var that = this
    var url = "/api/v1/stadium"
    var data = {}
    data.pageSize = that.data.pageSize
    data.pageNum = pageNum

    request.getRequest(url,data).then(res => {
      if (res.data.resultCode == 20000) {
       
        var datas = res.data.result.rows
        console.log(datas)
        var size = that.data.pageSize
        var totalNum = res.data.result.total
        var totalPage = totalNum % size == 0 ? parseInt(totalNum / size) : parseInt(totalNum / size) + 1
      

         for(var i=0;i<datas.length;i++){
           datas[i].juli = Number(map.juli(that.data.currentLatitude, that.data.currentLongitude, datas[i].latitude, datas[i].longitude)).toFixed(1)
           datas[i].checked=false
         }
       

        if (totalNum == 0) {
          that.setData({
            listGyms: datas,
            loading: { show: false },
            loading2: { show: false },
          })
        } else if (totalNum <= size) {
          datas.sort(function (a, b) {
            return a.juli - b.juli;
          })
          that.setData({
            totalPage: totalPage,
            listGyms: datas,
            pageNum: 1,
            loading: { show: false },
            loading2: { show: false },
          })
        } else {
          if (pageNum == 1) {
            datas.sort(function (a, b) {
              return a.juli - b.juli;
            })
            that.setData({
              totalPage: totalPage,
              listGyms: datas,
              isScrolling: false,
              loadingend: false,
              pageNum: 1,
              loading: { show: false },
              loading2: { show: false },
              config: [{
                type: 'loading',
                custom: false,
                line: true,
                color: '',
                loadingText: '加载中...',
                endText: '我是有底线的~'
              }]
            })
          }

          if (pageNum > 1 && pageNum <= totalPage) {
            var tempArray = that.data.listGyms;
              tempArray = tempArray.concat(datas);
              tempArray.sort(function (a, b) {
                return a.juli - b.juli;
              })
              that.setData({
                totalPage: totalPage,
                listGyms: tempArray,
                isScrolling: false,
                loadingend: false,
                loading: { show: false },
                loading2: { show: false },
              })
          
          }
        }
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  addGym(){
    var that = this
    wx.showToast({
      title: '添加中',
      image: "../../../img/loading_one.gif"
    })
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/stadium/add"
    var data={}
    var address = that.data.address
    data.creator=user_id
    data.stadiumName=that.data.name
    data.stadiumAddress=address
    data.longitude = that.data.longitude
    data.latitude = that.data.latitude
    var area = that.getArea(address)
    data.province = area.Province
    data.city = area.City
    data.district = area.District
    request.postRequest(url,data).then(res => {   
      if (res.data.resultCode == 20000) {
        that.onHidePopupTap() //隐藏添加页面
        that.listGym(1) //刷新列表
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  // 打开购物车菜单栏
  onSlideOpenTap(e) {
    const index = e.currentTarget.dataset.id
    this.setData({
      activeIndex: index
    });
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  //省市区截取
  getArea: function (str) {
    let area = {}
    let index11 = 0
    let index1 = str.indexOf("省")
    if (index1 == -1) {
      index11 = str.indexOf("自治区")
      if (index11 != -1) {
        area.Province = str.substring(0, index11 + 3)
      } else {
        area.Province = str.substring(0, 0)
      }
    } else {
      area.Province = str.substring(0, index1 + 1)
    }

    let index2 = str.indexOf("市")
    if (index11 == -1) {
      area.City = str.substring(index11 + 1, index2 + 1)
    } else {
      if (index11 == 0) {
        area.City = str.substring(index1 + 1, index2 + 1)
      } else {
        area.City = str.substring(index11 + 3, index2 + 1)
      }
    }

    let index3 = str.lastIndexOf("区")
    if (index3 == -1) {
      index3 = str.indexOf("县")
      area.District = str.substring(index2 + 1, index3 + 1)
    } else {
      area.District = str.substring(index2 + 1, index3 + 1)
    }
    if (area.Province == "") {
      area.Province = area.City
    }
    return area;
  }

})