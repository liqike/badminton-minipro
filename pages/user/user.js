// pages/user/user.js
const util = require('../../utils/util')
const request = require('../../utils/requestApi')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "编辑信息",
    entitle: "Edit Message",
    bgcolor: "",
    share: false,
    userinfo:{},
    phone:"",


    shengaos: [],
    sgIndex:23,

    tizhongs: [],
    tzIndex:20,

    qiupais:[],
    paishou:"zuoshou",
    paindex:0,

    chushengriqi: "",
    daqiunian: util.formatDate(new Date()),

    cityName:"上海-上海市-闵行区",

    showSkeleton: true,

    user:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.setStorageSync("page", "../index/index?select=0")
    /**
    * 获取系统信息
    */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight,
          userinfo:wx.getStorageSync("userInfo"),
          phone: wx.getStorageSync("phone")
        });
      }
    })

    // setTimeout(() => {

    // }, 1000)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getShenGaoData()
    this.getTizhongData()
    this.getUserInfo()
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getUserInfo(){
    var that=this
    var id=wx.getStorageSync("userInfo").userId
    var url ="/api/v1/user/getUserInfo/"+id
    request.getRequest(url).then(res => {    //！！！注意括号的个数！！！
      console.log(res)
      if (res.data.resultCode==20000){
        var user = res.data.result
        var shengaos = that.data.shengaos
        var tizhongs = that.data.tizhongs
        var qiupais = that.data.qiupais

        var index = shengaos.indexOf(user.height)
        var tiindex = tizhongs.indexOf(user.weight)
        var paindex = qiupais.indexOf(user.handedness)

        that.setData({
          user: user,
          sgIndex: index == -1 ? 23 : index,
          tzIndex: tiindex == -1 ? 21 : tiindex,
          paindex: paindex == -1 ? 0 : paindex,
          chushengriqi: user.birthday == null ? util.formatDate(new Date()) : user.birthday,
          daqiunian: user.badSYear == null ? util.formatDate(new Date()) : user.badSYear,
          showSkeleton: false
        })
      }else{
         request.showRequestError(res)
      }
    }).catch(res => {   
      request.showError()
    })
  },
  changePai(e){
     console.log(e)
    var idnex = e.detail.index == 0 ? "zuoshou":"youshou"
     this.setData({
       paishou:idnex,
       index: e.detail.index
     })
  },
  getShenGaoData(){
    var that = this

    var data = []
    var qiupai=[]
    qiupai.push("左手")
    qiupai.push("右手")
    for (var i = 150; i < 210; i++) {
      var sh = i + "cm"
      data.push(sh)
    }
    that.setData({
      shengaos: data,
      qiupais: qiupai
    })
  },
  getTizhongData() {
    var that = this

    var data = []
    for (var i = 40; i < 100; i++) {
      var sh = i + "kg"
      data.push(sh)
    }
    
    that.setData({
      tizhongs: data
    })
  },
})