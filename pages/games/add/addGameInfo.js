// pages/games/add/addGameInfo.js
const util = require('../../../utils/util')
const qrcodeUtil = require('../../../utils/qrcodeUtil')
const request = require('../../../utils/requestApi')
const map = require("../../../utils/map.js");

Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "活动打卡",
    winWidth: 0,
    winHeight: 0,
    height: 0,
    gym:{},
    game:{},
    list:{},
    startTime: "",
    week:"",
    times:[],
    evtName:"",
    evtHostName:"",
    timeindex: 1,
    bgImage:{},
    
    types:['娱乐活动'],
    typeIndex:0,

    prices:[],
    priceIndex:10,
    minValue:0,
    maxValue:100,
    showValue:0,
    code: false,
  
    config: {
      show: true,
      title: "活动创建成功,快去分享吧!",
      icon: "face",
      iconStyle: "size:60; color:#00C292",
      image: "",
      imageStyle: "",
      placement: '',
      duration: 1500,
      center: false,
      mask: false
    },
    config2: {
      show: true,
      title: "活动修改成功！",
      icon: "face",
      iconStyle: "size:60; color:#00C292",
      image: "",
      imageStyle: "",
      placement: '',
      duration: 1500,
      center: false,
      mask: false
    },
    currentConf: {

    },
    navConfig: {
      title: "退出系统",
      type: 1,
      config: {
        show: true,
        type: "confirm",
        showTitle: true,
        title: "创建活动",
        content: "确认打卡吗？",
        confirmText: '确定',
        confirmColor: '#3683d6',
        cancelText: '取消',
        cancelColor: '#999'
      }
    },
    currentConf2: {

    },
    gameId:"",
    commit: true,
    gameInfo:{},

    loading: {
      type: 'circle',
      show: true,
      size: 'default',
      color: '#34bfa3',
      name: 'circle'
    },
    imgUrls:[],
    titleItemList: [
      { name: '个人活动', color: 'black' }
    ],

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    var id = options.id == null ? 0 : options.id
 
    if(id!=0){
      that.setData({
        gameId: id,
        commit: false,
        code:true,
        title:"修改活动"
      })
      this.getGameInfo(id)
    }else{
      that.setData({
        list:{
          isCreator:true
        },
        startTime: util.formatTime(new Date()),
        week: util.getWeek(new Date()),
        loading: { show: false },
        evtHostName: this.data.titleItemList[0].name,
        evtName: util.formatDateEN(new Date()) + this.data.titleItemList[0].name
      })
      
    }
  /**
  * 获取系统信息
  */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight - (res.statusBarHeight * 2 + 20),
          height: res.statusBarHeight,
          show: true
        });
      }
    })

    this.getTimesInfo() //获取时长
    this.getPriceInfo() //获取费用列表
    if (id == 0) {
      map.getUserLocation().then(data => {
        var options = data[0]
        this.setData({
          currentLatitude: options.latitude,
          currentLongitude: options.longitude
        })
        this.listGym()
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
     
  },
  
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },
  getGameInfo(id){
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/standbylist/gameList"
    var data={}
    data.id = id
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows[0].tournaPersonal
        var list = res.data.result.rows[0]
        var priceIndex = that.data.prices.indexOf(list.fee == null ? 0 : list.fee)
        that.setData({
          game:datas,
          list:list,
          priceIndex: priceIndex,
          showValue: list.intensity == null ? 0 : list.intensity,
          startTime: util.formatTime(new Date(datas.evtSTime.replace(/-/g, '/'))),
          evtHostName: datas.evtHostName,
          evtName: datas.evtName,
          timeindex: datas.evtDuration-1,
          week: util.getWeek(datas.evtSTime.replace(/-/g, '/')),
          bgImage: {
            fileUrl: res.data.result.rows[0].fileUrl
          },
          gym: {
            id: datas.evtStadium,
            stadiumName: datas.evtStaName,
            stadiumAddress: datas.evtAddress
          },
          loading: { show: false }
        })
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  getTimesInfo(){
    var dates=[]
    for(var i=1;i<=24;i++){
      dates.push(i)
    }
    this.setData({
      times:dates
    })
  },
  getPriceInfo() {
    var dates = []
    for (var i = 0; i <= 150; i+=5) {
      dates.push(i)
    }
    this.setData({
      prices: dates
    })
  },
  chooseGym: function (e) {
    if (this.data.list.isCreator) {
      wx.navigateTo({
        url: '../../gymnasium/list/gymList',
      })
    }
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
      is_update_data: false
    })
  },
  dataChange:function(e){
    this.setData({
      timeindex:e.detail.index
    })
  },
  dataPriceChange:function(e){
    this.setData({
      priceIndex: e.detail.index
    })
  },
  dataType: function (e) {
    this.setData({
      typeIndex: e.detail.index
    })
  },
  getTimes(e){
    var evtName=util.formatDateEN(new Date(e.detail.startTime.replace(/\-/g, "/"))) + this.data.titleItemList[0].name + "活动"
    this.setData({
      startTime: e.detail.startTime.replace(/\-/g, "/"),
      week: e.detail.week,
      evtName: evtName
    })
  },
  getClubName(e){
    this.setData({
      evtHostName: e.detail.detail.value
    })
  },
  getevtName(e) {
    this.setData({
      evtName: e.detail.detail.value
    })
  },
  clearClubName(e){
    this.setData({
      evtHostName: ""
    })
  },
  sliderchange(e){
    this.setData({
      showValue: e.detail
    })
  },
  clearevtName(e) {
    this.setData({
      evtName: ""
    })
  },
  go_imageList(){
    if(this.data.list.isCreator){
      wx.navigateTo({
        url: '/pages/image/imageList',
      })
    }
  },
  listGym() {
    var that = this
    var url = "/api/v1/stadium"
    request.getRequest(url).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows
        for (var i = 0; i < datas.length; i++) {
          datas[i].juli = Number(map.juli(that.data.currentLatitude, that.data.currentLongitude, datas[i].latitude, datas[i].longitude)).toFixed(1)
          datas[i].checked = false
        }
        datas.sort(function (a, b) {
          return a.juli - b.juli;
        })
        console.log(datas[0])
        var gym={}
        gym.id = datas[0].id
        gym.stadiumAddress = datas[0].stadiumAddress
        gym.stadiumName = datas[0].stadiumName
        that.setData({
          gym: gym
        })
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  formSubmit(e) {
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
    this.setData({
      currentConf2: config
    })
  },
  onConfirmTap(e) {
    this.submithuo()
  },
  //确认打卡
  submithuo(){
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/personal/add"
    var data = {}
    data.creator = user_id
    var date = that.data.startTime
    if(date!=""){
      data.evtSTime = new Date(date.replace(/\-/g, "/"))
    }
    data.evtDuration= that.data.times[that.data.timeindex]
    data.evtStadium=that.data.gym.id
    data.evtHostName = that.data.evtHostName
    data.evtName = that.data.evtName
    data.evtType = 0 //数据字典：0、娱乐场；1、循环赛；2、淘汰赛；3、循环淘汰赛；4、八人转
    // data.evtSystem = 0 //0、无；1、循环赛；2、淘汰赛；3、循环淘汰赛；4、八人转；5、斗牛
    data.evtStaName = that.data.gym.stadiumName
    data.evtAddress = that.data.gym.stadiumAddress
    console.log(data)
    var msg=that.rules(data)
    if (msg!=""){
     that.showError(msg)
     return false
    }

    wx.showToast({
      title: '打卡中',
      image: "../../../img/loading_one.gif"
    })
    request.postRequest(url, data).then(res => {
      wx.hideToast()
    if (res.data.resultCode == 20000) {
      console.log(res)
      const config = JSON.parse(JSON.stringify(this.data.config))
      this.setData({
        currentConf: config
      })
      data.startTime = data.evtSTime
      data.week = util.getWeek(data.evtSTime)
      data.evtId= res.data.result
      var game = JSON.stringify(data)
      setTimeout(() => {
        wx.reLaunch({
          url: '../share/share?game=' + game+"&add="+1,
        })
      }, 1200)
    } else {
      wx.hideToast()
      request.showRequestError(res)
    }
  }).catch(res => {
    wx.hideToast()
    request.showError()
  })

  },
  update(){
    var that = this
    var list=that.data.list
    wx.showToast({
      title: '修改中...',
      image: "../../../img/loading_one.gif"
    })
    var data = {}
    if(list.isCreator){
      var game = that.data.game
      data.id = game.id
      var url = "/api/v1/personal"
      if (that.data.bgImage.id){
        data.evtBackground = that.data.bgImage.id
      }else{
        data.evtBackground = game.evtBackground
      }
      var date = that.data.startTime
      if (date != "") {
        data.evtSTime = new Date(date.replace(/\-/g, "/"))
      }
      // data.createDate = new Date(game.createDate.replace(/\-/g, "/"))
      data.evtDuration = that.data.times[that.data.timeindex]

      data.evtHostName = that.data.evtHostName
      data.evtName = that.data.evtName
      data.evtType = that.data.typeIndex //数据字典：0、娱乐场；1、循环赛；2、淘汰赛；3、循环淘汰赛；4、八人转
      // data.evtSystem = 0 //0、无；1、循环赛；2、淘汰赛；3、循环淘汰赛；4、八人转；5、斗牛

      data.evtStadium = that.data.gym.id
      data.evtStaName = that.data.gym.stadiumName
      data.evtAddress = that.data.gym.stadiumAddress
      data.listId=list.id
      data.showValue = that.data.showValue
      data.price = that.data.prices[that.data.priceIndex]
      console.log(data)
      request.putRequest(url, data).then(res => {
        wx.hideToast()
        if (res.data.resultCode == 20000) {
          const config2 = JSON.parse(JSON.stringify(this.data.config2))
          that.setData({
            currentConf: config2
          })
          setTimeout(() => {
            wx.navigateBack({
              delta: 1  // 返回上一级页面。
            })
          }, 400)

        } else {
          request.showRequestError(res)
        }
      }).catch(res => {
        wx.hideToast()
        request.showError()
      })
    }else{
      var url = "/api/v1/standbylist/update"
      data.id = list.id
      data.intensity = that.data.showValue
      data.fee = that.data.prices[that.data.priceIndex]
      request.putRequest(url, data).then(res => {
        wx.hideToast()
        if (res.data.resultCode == 20000) {
          const config2 = JSON.parse(JSON.stringify(this.data.config2))
          that.setData({
            currentConf: config2
          })
          setTimeout(() => {
            wx.navigateBack({
              delta: 1  // 返回上一级页面。
            })
          }, 400)

        } else {
          request.showRequestError(res)
        }
      }).catch(res => {
        wx.hideToast()
        request.showError()
      })
    }
   
  
   
  },
  rules(e){
    if (e.evtSTime == undefined || e.evtSTime == null) {
      return "活动时间不能为空"
    }

    if (e.evtHostName.replace(/(^\s*)|(\s*$)/g, "") == ""){
       return "主办方名称不能为空"
    }

    if (e.evtStadium == undefined || e.evtStadium == null) {
      return "场馆不能为空"
    }

    // if (e.evtName.replace(/(^\s*)|(\s*$)/g, "") == "") {
    //   return "活动名称不能为空"
    // }

    return ""
    
  },
  showBaseActionSheet() {
   this._showActionSheet({ itemList: this.data.titleItemList, showCancel: true })
  
  },
  _showActionSheet({ itemList, showCancel = false, title = '', locked = false }) {
    // var that=this
    wx.lin.showActionSheet({
      itemList: itemList,
      showCancel: showCancel,
      title: title,
      locked,
      success: (res) => {
        console.log(res);
        this.setData({
          evtHostName: res.item.name
        })
        // wx.showToast({
        //   title: res.item.name,
        //   icon: 'none'
        // })
      },
      fail: (res) => {
        console.log(res)
      }
    })
  },
  showError(msg){
    wx.lin.showMessage({
      type: 'error',
      duration: 2000,
      content: msg
    })
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  getqrcode(){
    var that = this
    var listId = that.data.gameId
    var data={}
    data.page ='pages/games/share/share'
    data.scene = listId
    wx.showLoading({
      title: '加载中..',
    })
    qrcodeUtil.getqrcode(data)
  }
 

})