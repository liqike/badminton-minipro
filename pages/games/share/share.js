// pages/games/share/share.js
const login = require("../../../utils/login.js");
const util = require('../../../utils/util')
const app = getApp()
const request = require('../../../utils/requestApi')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "活动报名",
    winWidth: 0,
    winHeight: 0,
    height: 0,
    marginTop:0,
    isHeader:true,
    myselfList:{},
    isSignup:false,

    isBack:false,

    game:{},
    standList:{},
    personal:{},
    standLists:[],
    
    share:true,
    have_home:true,

    loading: {
      type: 'circle',
      show: true,
      size: 'default',
      color: '#34bfa3',
      name: 'circle'
    },
    navConfig: {
      config: {
        show: true,
        animation: "show",
        zIndex: 99,
        contentAlign: "bottom",
        locked: true,
      }
    },
    currentConf: {

    },
    add:-1,
    scene:'',
    listId:'',
    evtId:'',
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var that = this
      var listId = options.listId
       var evtId = options.evtId
      var add = options.add == null ? -1 : options.add //创建活动传递
      if (options.scene) {
        var scene = decodeURIComponent(options.scene)
        that.setData({
          scene: scene
        });
      } else {
        if (add == 1) {
          var game = JSON.parse(options.game)
          evtId = game.evtId
          that.setData({
            game: game,
            evtId: evtId,
            add:add
          });
        } else {
          that.setData({
            listId: listId,
            evtId: evtId
          });
        }
      }
      var is_btn = options.is_btn

      if (is_btn != null) {
        that.setData({
          isBack: true,
          have_home: false
        })
      }

      wx.getSystemInfo({
        success: function (res) {
          that.setData({
            winWidth: res.windowWidth,
            winHeight: res.windowHeight,
            height: res.statusBarHeight,
            marginTop: res.statusBarHeight * 2 + 30
          });
        }
      })
    

   
  
  },
  /**
   * 扫码进入查询参与的人员
   */
  getGameInfo(id) {
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/standbylist/gameList" 
    var data = {}
    data.id = id
    request.getRequest(url, data).then(res => {
      console.log(res)
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows[0].tournaPersonal
        console.log(datas)
        that.getShareInfo(datas.id)
        datas.week = util.getWeek(datas.evtSTime.replace(/-/g, '/'))
        datas.startTime = new Date(datas.evtSTime.replace(/-/g, '/')).getTime()
        that.setData({
          standList: res.data.result.rows[0],
          game: datas
        })
      } else {
        request.showRequestError(res)
     
      }
    }).catch(res => {
      request.showError()
    })
  },
  /**
   * 分享进入查询参与的人员
   */
  getShareInfo(evtId){
    var that = this
    var user = wx.getStorageSync("userInfo")
    var data={}
    data.evtId = evtId
    data.isSignup=1
    var url = "/api/v1/standbylist/list"
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows
        console.log(datas)
        for(var i=0;i<datas.length;i++){
          if (datas[i].userId == user.userId){
            that.setData({
              myselfList: datas[i],
              listId: datas[i].id
            })
           }
        }
        that.setData({
          standLists: datas,
          loading: { show: false }
        })
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  closeJoin(e){
    var that=this
    wx.showToast({
      title: '退出中...',
      image: "../../../img/loading_one.gif"
    })
    var game = that.data.game
    var user = wx.getStorageSync("userInfo")
    var url = "/api/v1/standbylist"
    var myselfList = that.data.myselfList
    var data={}
    data.id = myselfList.id
    request.putRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        if (game.creator == user.userId){
        that.getShareInfo(game.id) //刷新列表
        that.setData({
          myselfList: {
            isSignup: false
          }
        })
        }else{
          wx.reLaunch({
            url: '../../index/index?select=1',
          })

        }
     
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that=this
    var scene = this.data.scene
    var listId = this.data.listId
    var evtId = this.data.evtId
    var add = this.data.add
    if (scene!='') {
      this.getGameInfo(scene)
    } else {
      if (add == 1) {
        that.getShareInfo(evtId)
      } else {
        that.getGameInfo(listId)
      }
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    var listId = this.data.listId
    var evtId=this.data.evtId
    console.log(listId)
    console.log(evtId)

    return {
      title: '个人活动报名',
      path: '/pages/games/share/share?listId=' + listId + "&evtId=" + evtId,
      success: function () {
        this.setData({
          share: false
        })
      },
      fail: function () {
        console.log(res)
        // 转发失败之后的回调
　　　　 if (res.errMsg == 'shareAppMessage:fail cancel') {
        this.setData({
          share: true
        })
　　　　} else if (res.errMsg == 'shareAppMessage:fail') {
　　　　　　// 转发失败，其中 detail message 为详细失败信息
　　　　}
    }
   }
  },
  joinGame(){
    var that=this
    wx.showToast({
      title: '加入中...',
      image: "../../../img/loading_one.gif"
    })
    var user = wx.getStorageSync("userInfo")
    var game = that.data.game
    console.log(game)
    var url = "/api/v1/standbylist/add"
    var data = {}
    data.evtId = that.data.evtId
    data.userId = user.userId
    if (user.userId == game.creator) {
      data.isCreator = true
    } else {
      data.isCreator = false
    }
    data.isSignup = true
    data.creator = game.creator
    request.postRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        that.getShareInfo(that.data.evtId) //刷新列表
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  bindGetUserInfo: function (e) {
    var that = this
    if (wx.getStorageSync("userInfo")==''){
      wx.setStorageSync("userInfo", e.detail.userInfo);
      if (e.detail.errMsg == 'getUserInfo:fail auth deny') {
        wx.showModal({
          showCancel: false,
          content: '未授权',
          success: function (res) {
            wx.navigateBack({
              url: wx.getStorageSync("page")
            })
            wx.hideLoading()
          }
        })
      } else {
        that.onShowPopupTap()

      }
    } else if (wx.getStorageSync("token") == ''){
      that.onShowPopupTap()
    }else{
      that.joinGame()
    }
   
  },

  getPhoneNumber: function (e) {
    // console.log(e.detail.iv);
    // console.log(e.detail.encryptedData);
    var that = this

    if (e.detail.errMsg == 'getPhoneNumber:fail user deny') {
      wx.showModal({
        title: '提示',
        showCancel: false,
        content: '手机授权失败',
        success: function (res) {
        }
      })
    } else {
      wx.showLoading({
        title: '授权中...',
        mask: true
      })
      wx.request({
        url: app.globalData.host + '/api/v1/wxLogin/loginWithPhone',
        data: {
          sessionKey: wx.getStorageSync("sessionKey"),
          encryptedData: e.detail.encryptedData,
          iv: e.detail.iv
        },
        header: {
          "Content-Type": "application/x-www-form-urlencoded",// 默认值
        },
        method: 'POST',
        success: function (res) {
          wx.hideLoading()
          console.log(res)
          var code = res.data.resultCode
          if (code === 20000) {
            var realphone = res.data.result.userPhone
            var phone = realphone.substr(0, 3) + "****" + realphone.substr(7)
            wx.setStorageSync("realphone", realphone)
            wx.setStorageSync("phone", phone)
            that.onHidePopupTap()
            that.login(wx.getStorageSync("userInfo"))
          }
        },
        fail: function (res) {
          wx.hideLoading()
          console.log('is failed')
        }
      })
    }
  },
 login(detail) {
  var that = this
  var userInfo = detail
   wx.showLoading({
     title: '登录中...',
     mask: true
   })
 wx.login({
      success: res => {
        wx.request({
          url: app.globalData.host + '/api/v1/wxLogin/wxLogin',
          data: {
            wxCity: userInfo.city,
            wxNickname: userInfo.nickName,
            wxCountry: userInfo.country,
            wxImg: userInfo.avatarUrl,
            wxProvince: userInfo.province,
            wxGender: userInfo.gender,
            wxOpenid: wx.getStorageSync("openID"),
            userPhone: wx.getStorageSync("realphone"),
          },
          header: {
            'Content-type': 'application/json',// 默认值,
            'mobile': 't'
          },
          method: 'POST',
          success: function (res) {
            wx.hideLoading()
            console.log(res)
            var code = res.data.resultCode
            if (code == 20000) {
              wx.setStorageSync("userInfo", res.data.result.wxUsers)
              wx.setStorageSync("token", res.data.result.token)
              wx.setStorageSync("role", res.data.result.roles[0])
              that.joinGame()
            }else{
              wx.showModal({
                title: '提示',
                showCancel: false,
                content: '手机授权失败',
                success: function (res) {
                }
              })
            }
          },
          fail: function (res) {
            console.log('is failed')
          }
        })
      }
    })
  },
  // 显示Popup
  onShowPopupTap() {
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
    console.log(config)
    this.setData({
      currentConf: config,
      type: 2
    })
  },

  onHidePopupTap() {
    this.data.currentConf.show = false
    this.setData({
      currentConf: this.data.currentConf
    })
    // this.getGameInfo()
  },
})