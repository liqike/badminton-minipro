// pages/games/plays/player.js
const request = require('../../../utils/requestApi')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "选手",
    winWidth: 0,
    winHeight: 0,
    height: 0,
    isBack:true,
    isVs:false,

    evtId:'',
    playtype:-1,

    loading: {
      type: 'circle',
      show: true,
      size: 'default',
      color: '#34bfa3',
      name: 'circle'
    },

    plays: [],
    playsname:[],
    choosepeople:[],
    lengths:2,
    position: 'left',


  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var type=options.type
    var playtype = options.playtype
    var evtId = options.evtId
    var choosepeople = JSON.parse(options.choosepeople)
    var that=this

    if (type == 1 && playtype == 2){ //双打 对手
     that.setData({
       isVs:true
     })
    }

    /**
     * 获取系统信息
     */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight - (res.statusBarHeight * 2 + 20),
          height: res.statusBarHeight,
          choosepeople: choosepeople
        });
      }
    })

    that.setData({
      evtId: evtId,
      playtype: playtype
    })
    that.getPlayersInfo(evtId)
  },
  getPlayersInfo(evtId) {
    var that = this
    var userinfo = wx.getStorageSync("userInfo")
    var data = {}
    data.evtId = evtId
    data.isSignup = 1
    var url = "/api/v1/standbylist/list"
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows
        var choosepeople = that.data.choosepeople
        var users=[]
        var names=[]
        for (var i = 0; i < datas.length; i++) {
          if (userinfo.userId != datas[i].wxUsers.userId && choosepeople.indexOf(datas[i].wxUsers.wxNickname)==-1){
              var user = {}
              user.id = Number(datas[i].wxUsers.userId)
              user.name = datas[i].wxUsers.wxNickname
              user.img = datas[i].wxUsers.wxImg
              users.push(user) 
              names.push(datas[i].wxUsers.wxNickname)
          }

        }
        setTimeout(()=>{
          that.setData({
            plays: users,
            playsname: names,
            loading: { show: false }
          })
        },200)
         
      
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  /**
   * 多选
   */
  change(e) {
    var users=[]
    var all = e.detail.all
    var plays=[]
    var users=this.data.plays
    var playsnames = this.data.playsname
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    for(var i=0;i<all.length;i++){
      var choosepeople = this.data.choosepeople
      var name=all[i].value
       var index=playsnames.indexOf(name)
       plays.push(users[index])
      choosepeople.push(name)
    }
    if (all.length == this.data.lengths){
      prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
        opponents: plays,
        choosepeople: choosepeople
      })
      wx.navigateBack({
        delta: 1  // 返回上一级页面。
      })
    }
  
  },

  onChange(e) {
    var users=this.data.plays
    var index=e.detail.id
    var playtype = this.data.playtype
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    if (playtype==1){ //拍档
      var choosepeople = this.data.choosepeople
      var checkUser = users[index]
      var users = []
      users.push(checkUser)
      choosepeople.push(checkUser.name)
      prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
        partners: users,
        choosepeople: choosepeople
      })
    }
    if (playtype == 2) {
      var checkUser = users[index]
      var users = []
      users.push(checkUser)
      prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
        opponents: users,
      })
    }
    wx.navigateBack({
      delta: 1  // 返回上一级页面。
    })
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})