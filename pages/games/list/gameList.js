// pages/games/list/gameList.js
const app = getApp()
const request = require('../../../utils/requestApi')
import timeHandle from '../../../utils/timeHandle';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    title: "比赛列表",
    winWidth: 0,
    winHeight: 0,
    height: 0,
    isBack: true,
    isAdd: true,
    listId: "",
    evtId: "",
    name:"",
    index:0,
    isSignup:false,
    animation1:"",
    animation2: "",
    userId:'',
    teamId:'',
    homeId:'',
    awayId:'',
    is_update:true,

    types: ['单打','双打'],
    typeIndex: 1,

    checked:"",

    sArray:[],
    sTimes:[],

    partners:[],
    
    opponents:[],

    choosepeople:[],

    leftSore:0,
    rightScore:0,
    totalScore: '',
    leftTime:'',
    rightTime:'',
    totalTime:'',
    duration:'',
    gameVideo:'',
    gameSummary:'',
    len:0,
    is_addGames:false,
    update_data:{},
    standLists:[],
    show_people:false,
    
    navConfig: {
      config: {
        show: true,
        animation: "show",
        zIndex: 99,
        contentAlign: "bottom",
        locked: true,
      }
    },
    currentConf: {

    },
    // navConfig2: {
    //   config: {
    //     show: true,
    //     animation: "show",
    //     zIndex: 99,
    //     contentAlign: "top",
    //     locked: true,
    //   }
    // },
    // currentConf2: {

    // },
    loading: {
      type: 'circle',
      show: true,
      size: 'default',
      color: '#34bfa3',
      name: 'circle'
    },
    titleItemList: [
      { name: '删除比赛', color: '#F4516C' }
    ],
    leftData:[],

    isScrolling: false,
    loadingend: false,
    is_update_data:true,
    scrollTop: 0,
    pageNum: 1,//页码
    pageSize: 10, //每页的查询数据
    totalPage: 0, //共计页数
    config: [{
      type: 'loading',
      custom: false,
      line: true,
      color: '',
      loadingText: '加载中...',
      endText: '我是有底线的~'
    }
    ],
    loading2: {
      type: 'circle',
      show: false,
      size: 'default',
      color: 'black',
      name: 'circle'
    },
    scrollLeft:0,
    loading3: {
      type: 'circle',
      show: false,
      size: 'default',
      color: 'black',
      name: 'circle'
    },

    isIphoneX:false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this
    var that = this
    var listId = options.listId == null ? 0 : options.listId
    var evtId = options.evtId == null ? 0 : options.evtId
    var index = options.index
    var isSignup = options.isSignup == "false" ? false : options.isSignup
    var name = options.name == null ? 0 : options.name
    /**
      * 获取系统信息
      */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight - (res.statusBarHeight * 2 + 20),
          height: res.statusBarHeight,
          show: true,
          listId: listId,
          evtId: evtId,
          name: name,
          index: index,
          isSignup: isSignup,
          userId: wx.getStorageSync("userInfo").userId
        });
      }
    })
    this.getmultiArray()
    this.getTimesArray()
    // app.showScale(this, 'img',1.4)
  },

  _showActionSheet({ itemList, showCancel = false, index, title = '', locked = false }) {
    wx.lin.showActionSheet({
      itemList: itemList,
      showCancel: showCancel,
      title: title,
      locked,
      success: (res) => {
        var that = this
        var url = "/api/v1/games/"
        request.deleteRequest(url, index).then(res => {
          if (res.data.resultCode == 20000) {
            this.getTeamsInfo(this.data.evtId,1)
          } else {
            request.showRequestError(res)
          }
        }).catch(res => {
          request.showError()
        })
        wx.showToast({
          title: '删除成功',
          icon: 'none'
        })
      },
      fail: (res) => {
        console.log(res)
      }
    })
  },
  dataChange: function (e) {
    this.setData({
      typeIndex: e.detail.index
    })
    if (!this.data.is_update && e.detail.index==1){
         this.setData({
           partners:[]
         })
    }

    if (!this.data.is_update && e.detail.index == 0) {
      this.setData({
        opponents: [],
        partners: []
      })
    }
  },
  getScore(e){
    this.setData({
      leftSore: Number(e.detail.left),
      rightScore: Number(e.detail.right)
    })
  },
  getTimes(e) {
    this.setData({
      duration: e.detail.left + e.detail.right
    })
  },
  getGameVideo(e){

    this.setData({
      gameVideo: e.detail.detail.value
    })
  },
  getGameSummary(e) {
    var value = e.detail.value
    var len = parseInt(value.length);
    this.setData({
      gameSummary: value,
      len: len
    })
  },
  getmultiArray(){
    var multiArray=[]
    var data=[]
    for(var i=0;i<=30;i++){
         data.push(i+'')
    }
    multiArray.push(data)
    multiArray.push([':'])
    multiArray.push(data)
    this.setData({
      sArray: multiArray
    })
  },
  getTimesArray() {
    var multiArray = []
    var data1 = []
    var data2 = []
    for (var i = 0; i <= 60; i++) {
      data1.push(i + "'")
    }
    for (var i = 0; i <= 60; i++) {
      data2.push(i + '"')
    }
    multiArray.push(data1)
    multiArray.push(['-'])
    multiArray.push(data2)
    this.setData({
      sTimes: multiArray
    })
  },
  showBaseActionSheet(e) {
    var user_id = wx.getStorageSync("userInfo").userId
    if (e.currentTarget.dataset.creator != user_id){
       return false;
    }else{
    this._showActionSheet({ itemList: this.data.titleItemList, showCancel: true, index: e.currentTarget.dataset.id})
    }
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    if (!this.data.is_addGames){
      this.setData({
        loading2: { show: true },
      })
      this.getTeamsInfo(this.data.evtId,1)
     
    }
  },
  search_people(e){
    var userId=e.currentTarget.dataset.id
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/games/teamList"
    var data = {}
    data.evtId = that.data.evtId
    data.pageSize = 100
    data.pageNum = 1
    that.setData({
      loading2: { show: true },
      leftData:[],
      loadingend: false,
    })
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows
         console.log(datas)
         var fileterData=[]
        for (var i = 0; i < datas.length; i++) {

          datas[i].duration = this.parseDuration(datas[i].duration)
          if (datas[i].event=="7"){
            if (userId == datas[i].opponents[0].id || userId == datas[i].partners[0].id) {
              fileterData.push(datas[i])
            }
          }
          if (datas[i].event == "6") {
            if (userId == datas[i].opponents[0].id || userId == datas[i].opponents[1].id || userId == datas[i].partners[0].id || userId == datas[i].partners[1].id) {
              fileterData.push(datas[i])
            }
          }
         
        }

        that.setData({
          animation1: "animated fadeInLeft  delay-0.5s ",
          animation2: "animated fadeInRight delay-0.5s  ",
          leftData: fileterData,
          pageNum:1,
          isScrolling:true,
          isIphoneX:true,
          loadingend: true,
          loading2: { show: false },
          config: [{
            type: 'end',
            custom: false,
            is_update_data: false,
            line: true,
            color: '',
            loadingText: '加载中...',
            endText: '我是有底线的~'
          }]
        })

      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  getShareInfo(evtId, pageNum) {
    var that = this
    var user = wx.getStorageSync("userInfo")
    var data = {}
    data.evtId = evtId
    data.isSignup = 1
    data.pageSize = 100
    data.pageNum = pageNum
    var url = "/api/v1/standbylist/list"
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas = res.data.result.rows
        console.log(datas)
        for (var i = 0; i < datas.length; i++) {
          if (datas[i].userId == user.userId) {
            that.setData({
              myselfList: datas[i],
              listId: datas[i].id
            })
          }
        }
        that.setData({
          standLists: datas,
          loading3: { show: false }
        })
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },
  scroll: function (event) {
    this.setData({
      scrollTop: event.detail.scrollTop
    });
  },
 
  // 下拉刷新
  refresh: function (e) {
    var that = this;
    // console.log(that.data.scrollTop)
    if (that.data.scrollTop < -10) {
      that.setData({
        pageNum: 1,
        loading: { show: true },
        loading2: { show: true },
        isScrolling: false,
        config: [{
          type: 'loading',
          custom: false,
          line: true,
          color: '',
          loadingText: '加载中...',
          endText: '我是有底线的~'
        }]
      })
      setTimeout(function () {
        that.getTeamsInfo(that.data.evtId, 1)
      }, 500);
    }
  },
  //上拉
  getOthers: function (e) {
    var that = this
    if (this.data.isScrolling === true)
      return;

    that.setData({
      isScrolling: true,
      loadingend: true,
      is_update_data: true
    })
    if (that.data.pageNum == that.data.totalPage) {
      that.setData({
        config: [{
          type: 'end',
          custom: false,
          is_update_data: false,
          line: true,
          color: '',
          loadingText: '加载中...',
          endText: '我是有底线的~'
        }]
      })
    } else {
      that.setData({
        pageNum: that.data.pageNum + 1,
      })
      setTimeout(() => {
        that.getTeamsInfo(this.data.evtId, that.data.pageNum)
      }, 300)
    }

  },
  getTeamsInfo(evtId,pageNum) {
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/games/teamList"
    var data = {}
    data.evtId = evtId
    data.pageSize = that.data.pageSize
    data.pageNum = pageNum
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
        var datas=res.data.result.rows
        var size = that.data.pageSize
        var totalNum = res.data.result.total
        var totalPage = totalNum % size == 0 ? parseInt(totalNum / size) : parseInt(totalNum / size) + 1
        for(var i=0;i<datas.length;i++){
          datas[i].duration = this.parseDuration(datas[i].duration)
        }

        // that.setData({
        //   animation1: "animated fadeInLeft  delay-0.5s ",
        //   animation2: "animated fadeInRight delay-0.5s  ",
        //   leftData: datas,
        //   loading: { show: false }
        // })

        if (totalNum == 0) {
          that.setData({
            leftData: datas,
            loading: { show: false },
            loading2: { show: false },
          })
        } else if (totalNum <= size) {
          that.setData({
            animation1: "animated fadeInLeft  delay-0.5s ",
            animation2: "animated fadeInRight delay-0.5s  ",
            totalPage: totalPage,
            leftData: datas,
            pageNum: 1,
            loading: { show: false },
            loading2: { show: false },
          })
        } else {
          if (pageNum == 1) {
            that.setData({
              animation1: "animated fadeInLeft  delay-0.5s ",
              animation2: "animated fadeInRight delay-0.5s  ",
              totalPage: totalPage,
              leftData: datas,
              isScrolling: false,
              loadingend: false,
              pageNum: 1,
              loading: { show: false },
              loading2: { show: false },
              config: [{
                type: 'loading',
                custom: false,
                line: true,
                color: '',
                loadingText: '加载中...',
                endText: '我是有底线的~'
              }]
            })
          }

          if (pageNum > 1 && pageNum <= totalPage) {
            if (that.data.is_update_data) {
              var tempArray = that.data.leftData;
              tempArray = tempArray.concat(datas);
              console.log(tempArray)
              that.setData({
                animation1: "animated fadeInLeft  delay-0.5s ",
                animation2: "animated fadeInRight delay-0.5s  ",
                totalPage: totalPage,
                leftData: tempArray,
                isScrolling: false,
                loadingend: false,
                loading: { show: false },
                loading2: { show: false },
              })
            }
          } 
        }
      } else {
        request.showRequestError(res)
      }
    }).catch(res => {
      request.showError()
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
   
  },
  // 显示Popup
  onShowPopupTap() {
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
    this.setData({
      currentConf: config,
      type: 2,
      isBack: false,
      isSignup: false,
      title: "添加比赛",
      is_addGames:true
    })
  },
  // // 显示 search Popup
  go_search() {
    // console.log(11111)
    // const config = JSON.parse(JSON.stringify(this.data.navConfig2.config))
    if(this.data.show_people){
      this.setData({
        show_people: false
      })
    }else{
        this.setData({
          show_people: true,
          loading3:{show:true}
        })
       this.getShareInfo(this.data.evtId,1)
    }
  },

  onHidePopupTap() {
    this.data.currentConf.show = false
    this.setData({
      currentConf: this.data.currentConf,
      isBack: true,
      isSignup: true,
      is_update:true,
      title: "比赛列表",
      typeIndex:1,
      totalScore:'',
      totalTime:'',
      partners:[],
      opponents:[],
      checked:'',
      gameVideo:'',
      gameSummary:'',
      choosepeople:[],
      is_addGames: false
    })
  },
  choosePlayer(e){
    var type = this.data.typeIndex
    var evtId = this.data.evtId
    var choosepeople = JSON.stringify(this.data.choosepeople)
    var playtype = e.currentTarget.dataset.playtype

    wx.navigateTo({
      url: '../players/player?type=' + type + '&playtype=' + playtype + '&evtId=' + evtId + '&choosepeople=' + choosepeople,
    })
  },
  switchChange(e){
    var checked=e.currentTarget.dataset.checked
    if(checked==""){
      this.setData({
        checked:"checked"
      })
    }else{
      this.setData({
        checked: ""
      })
    }
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    let pages = getCurrentPages();
    let prevPage = pages[pages.length - 2];
    prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
      is_update_data: false,
      update_data:this.data.update_data
    })
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  createTeam(){
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/games/add"
    var data = {}
    data.creator = user_id
    data.evtId=this.data.evtId
    data.event = this.data.typeIndex 
    data.isHandicap = this.data.checked==""? false:true
    data.duration = this.data.duration
    data.homeScore = this.data.leftSore //主队比分
    data.awayScore = this.data.rightScore //客队比分
    
    if (this.data.leftSore == this.data.rightScore){
      data.gameResult = '1'
    } else if (this.data.leftSore > this.data.rightScore){
      data.gameResult = '3'
    }else{
      data.gameResult = '0'
    }

    data.duration = this.getDuration(this.data.duration)
   
    data.gameVideo = this.data.gameVideo
    data.gameSummary = this.data.gameSummary
    
    var partners = this.data.partners
    var myslef={}
    myslef.id = user_id
    myslef.name = wx.getStorageSync("userInfo").wxNickname
    partners.push(myslef)

    data.partners = partners//对友
    data.opponents = this.data.opponents //对手

    var msg = this.rules(data)
    if (msg != "") {
      this.showError(msg)
      return false
    }
    var json=JSON.stringify(data)
    wx.showToast({
      title: '创建比赛',
      image: "../../../img/loading_one.gif"
    })
    request.postJsonRequest(url, json).then(res => {
      wx.hideToast()
      if (res.data.resultCode == 20000) {
        this.onHidePopupTap()
        this.getTeamsInfo(this.data.evtId,1)
        this.listGames(this.data.evtId)
        this.setData({
          partners:[],
          opponents:[],
          is_addGames:false
        })
        // let pages = getCurrentPages();
        // let prevPage = pages[pages.length - 2];
        // prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
        //   pageNum: 1
        // })
      } else {
        wx.hideToast()
        request.showRequestError(res)
      }
    }).catch(res => {
      wx.hideToast()
      request.showError()
    })
  },
  goUpdateTeam(e){
    var user_id = wx.getStorageSync("userInfo").userId
    if (e.currentTarget.dataset.creator == user_id) {
    const config = JSON.parse(JSON.stringify(this.data.navConfig.config))
    this.setData({
      currentConf: config,
      type: 2,
      isBack: false,
      isSignup: false,
      title: "修改比赛"
    })
    var index = e.currentTarget.dataset.id
    var datas=this.data.leftData
    var data=datas[index]
    console.log(data)
    var tyindex = data.event == '7' ? 0 : data.event == '6'?1:-1
    var pars=[]
   

    if(tyindex==1){
      for (var i = 0; i < data.partners.length; i++) {
        if (data.partners[i].id != this.data.userId) {
          pars.push(data.partners[i])
        }
      }
      this.setData({
        opponents: data.opponents,
        partners: pars
      })
    }

    if (tyindex == 0) {
      var opens=[]
      for (var i = 0; i < data.opponents.length; i++) {
        if (data.opponents[i]!= null) {
          opens.push(data.opponents[i])
        }
      }
      console.log(opens)
      this.setData({
        opponents: opens,
        partners: []
      })
    }
    // var user_id = wx.getStorageSync("userInfo").userId
    // if (data.creator == user_id){
    //   this.setData({
    //      disable:''
    //   })
    // }else{
    //   this.setData({
    //     disable: 'disable'
    //   })
    // }
    
    this.setData({
      typeIndex:tyindex,
      leftSore: data.homeScore,
      rightScore: data.awayScore,
      totalScore: data.homeScore + ":" + data.awayScore,
      rightTime: Number(data.duration.substring(0, data.duration.indexOf("'"))),
      leftTime: Number(data.duration.substring(data.duration.indexOf("'") + 1, data.duration.indexOf('"'))) ,
      totalTime: data.duration,
      checked: data.isHandicap ?"checked":'',
      gameVideo: data.gameVideo,
      gameSummary: data.gameSummary,
      len: parseInt(data.gameSummary.length),
      is_update:false,
      teamId:data.id,
      homeId: data.homeId,
      awayId: data.awayId
    })

   
    }
  },
  updateTeam(){
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/games"
    var data = {}
    data.id = this.data.teamId
    data.creator = user_id
    data.evtId = this.data.evtId
    data.event = this.data.typeIndex
    data.isHandicap = this.data.checked == "" ? false : true
    data.homeId = this.data.homeId
    data.awayId = this.data.awayId
    data.homeScore = this.data.leftSore //主队比分
    data.awayScore = this.data.rightScore //客队比分

    if (this.data.leftSore == this.data.rightScore) {
      data.gameResult = '1'
    } else if (this.data.leftSore > this.data.rightScore) {
      data.gameResult = '3'
    } else {
      data.gameResult = '0'
    }

    data.duration = this.getDuration(this.data.totalTime)

    data.gameVideo = this.data.gameVideo
    data.gameSummary = this.data.gameSummary

    var partners = this.data.partners

    var myslef = {}
    myslef.id = user_id
    myslef.name = wx.getStorageSync("userInfo").wxNickname
    partners.push(myslef)
    
   

    data.partners = partners//对友
    data.opponents = this.data.opponents //对手

    var msg = this.rules(data)
    if (msg != "") {
      this.showError(msg)
      return false
    }
    var json = JSON.stringify(data)
    wx.showToast({
      title: '修改比赛',
      image: "../../../img/loading_one.gif"
    })

    request.putJsonRequest(url, json).then(res => {
      wx.hideToast()
      if (res.data.resultCode == 20000) {
        wx.showToast({
          title: '修改成功',
          duration:1000,
          image: "../../../img/loading_one.gif"
        })
        this.onHidePopupTap()
        this.getTeamsInfo(this.data.evtId,1)
        this.listGames(this.data.evtId)

        this.setData({
          partners: [],
          opponents: [],
          is_addGames:false
        })
       
      } else {
        wx.hideToast()
        request.showRequestError(res)
      }
    }).catch(res => {
      wx.hideToast()
      request.showError()
    })
  },
  //生涯列表
  listGames(evtId) {
    var that = this
    var user_id = wx.getStorageSync("userInfo").userId
    var url = "/api/v1/standbylist/gameList"
    var data = {}
    // data.isSignup = 1
    data.userId = user_id
    // data.pageSize = 10
    // data.pageNum = 1
    data.evtId = evtId
    request.getRequest(url, data).then(res => {
      if (res.data.resultCode == 20000) {
       
        var datas = res.data.result.rows
        var size = that.data.pageSize
        var totalNum = res.data.result.total

        datas.forEach(item => {
          item.tournaPersonal.evtSTime = timeHandle.timeHandle(new Date(item.tournaPersonal.evtSTime.replace(/-/g, "/")))
          item.tournaPersonal.createDate = timeHandle.timeHandle(new Date(item.tournaPersonal.createDate.replace(/-/g, "/")))
        });
        console.log(datas)
        that.setData({
          update_data: datas[0]
        })
        // let pages = getCurrentPages();
        // let prevPage = pages[pages.length - 2];
        // prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
        //   update_data: datas[0]
        // })
      } else {
        // wx.hideToast()
        request.showRequestError(res)
      }
    }).catch(res => {
      // wx.hideToast()
      request.showError()
    })
  },
  getDuration(e){
    var s = Number(e.substring(0,e.indexOf("'"))*60)
    var m = Number(e.substring(e.indexOf("'")+1, e.indexOf('"'))) 
    return s+m
  },
  parseDuration(e) {
    var s = parseInt(e/60)
    var m = e%60
    return s+"'" + m+'"'
  },
  rules(e) {

    if (this.data.typeIndex == 0) {
      if (e.opponents.length == 0 || e.opponents == null) {
        return "对手不能为空"
      }
    }
    if (this.data.typeIndex == 1) {
      if (e.partners.length != 2 || e.partners.length  == 0) {
        return "搭档不能为空"
      }
      if (e.opponents.length == 0) {
        return "类型双打,对手不能为空"
      }
      if (e.opponents.length == 1 ) {
        return "类型为双打，对手不能为单人"
      }
    }
    if (e.awayScore == undefined || e.awayScore == -1) {
      return "比分不能为空"
    }

    if (e.duration == undefined || e.evtStadium == '') {
      return "耗时不能为空"
    }

    return ""

  },
  showError(msg) {
    wx.lin.showMessage({
      type: 'error',
      duration: 2000,
      content: msg
    })
  },
})