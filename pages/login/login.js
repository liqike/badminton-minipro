// pages/login/login.js
//获取应用实例
const app = getApp()
const login = require("../../utils/login.js");
Page({
  
  /**
   * 页面的初始数据
   */
  data: {
    winWidth: 0,
    winHeight: 0,
    height: 0,
    title: "微信授权登录",
    bgcolor: wx.getStorageSync("bg"),
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    wx.setStorageSync("page", "../index/index?select=0")
    /**
    * 获取系统信息
    */
    wx.getSystemInfo({
      success: function (res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight,
          height: res.statusBarHeight
        });
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  // 获取输入账号
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },

  // 获取输入密码
  passwordInput: function (e) {
    this.setData({
      password: e.detail.value
    })
  },

  bindGetUserInfo: function (e) {
    wx.setStorageSync("userInfo", e.detail.userInfo);
    if (e.detail.errMsg == 'getUserInfo:fail auth deny') {
      wx.showModal({
        showCancel: false,
        content: '未授权',
        success: function (res) {
          wx.navigateBack({
            delta: 1
          })
        }
      })
    }else{
      wx.navigateTo({
        url: '/pages/phone/phone',
      })
    }
  },

  // 登录
  login: function () {
    if (this.data.phone.length == 0 || this.data.password.length == 0) {
      wx.showToast({
        title: '用户名和密码不能为空',
        icon: 'loading',
        duration: 2000
      })
    } else {
      // 这里修改成跳转的页面
      wx.showToast({
        title: '登录成功',
        icon: 'success',
        duration: 2000
      })
      wx.switchTab  ({
        url: '../user/user'
      })

    }
  },
  reg: function(){
    console.log(12312312);
  }
})