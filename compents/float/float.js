// compents/float/float.js
Component({
  properties: {
    // 这里定义了innerText属性，属性值可以在组件使用时指定
    top: {
      type: String,
      value: '40%',
    },
    left: {
      type: String,
      value: '89%',
    }
  },
  data: {
    // 这里是一些组件内部数据
    // top: '80%',
    // left: '89%',
    isPopping: true,//是否已经弹出

    animPlus: {},//旋转动画

    animCollect: {},//item位移,透明度

    animTranspond: {},//item位移,透明度

    animInput: {},//item位移,透明度
  },
  lifetimes: {
    // 生命周期函数，可以为函数，或一个在methods段中定义的方法名
    attached: function () {
      var res = wx.getSystemInfoSync();
      this.setData({
        systemInfo: res,
      })
    },
    moved: function () { },
    detached: function () { },
  },

  methods: {
    // 这里是一个自定义方法
    //拖动不超过规定范围
    /**
     * 计算拖动的位置
     */
    setTouchMove: function (e) {
      var top = e.touches[0].clientY;
      var left = e.touches[0].clientX;
      var systemInfo = this.data.systemInfo;
      var maxTop = systemInfo.windowHeight * 0.78;
      var maxLeft = systemInfo.windowWidth * 0.89;
      if (top < systemInfo.windowHeight * 0.14) {
        top = systemInfo.windowHeight * 0.14;
      }
      if (top > maxTop) {
        top = maxTop;
      }
      if (left < 0) {
        left = 0;
      }
      if (left > maxLeft) {
        left = maxLeft;
      }
      this.triggerEvent('myevent', {
        top: top + "px",
        left: left + "px"
      });
    },
    setCoordinates: function () {
      var left = Number(this.data.left.replace("px", ""));
      if (left > (this.data.systemInfo.windowWidth / 2)) {
        left = this.data.systemInfo.windowWidth * 0.89;
      } else {
        left = 0;
      }
      this.triggerEvent('myevent', {
        top: this.data.top,
        left: left + "px",
      });
    },

    plus: function () {
     
      if (this.data.isPopping) {

       
        //弹出动画
        this.popp();

        this.setData({

          isPopping: false

        })

      } else if (!this.data.isPopping) {

   
       //缩回动画
        this.takeback();

        this.setData({

          isPopping: true

        })
      }

    },

    input: function () {

      console.log("input")

    },

    transpond: function () {

      console.log("transpond")

    },

    collect: function () {
      this.triggerEvent("addGameInfo")
      this.plus()
    },


    //弹出动画

    popp: function () {

      //plus顺时针旋转

      var animationPlus = wx.createAnimation({

        duration: 500,

        timingFunction: 'ease-out'

      })

      var animationcollect = wx.createAnimation({

        duration: 200,

        timingFunction: 'ease-out'

      })

      var animationTranspond = wx.createAnimation({

        duration: 420,

        timingFunction: 'ease-out'

      })

      var animationInput = wx.createAnimation({

        duration: 630,

        timingFunction: 'ease-out'

      })

      animationPlus.rotateZ(360).step();

      animationcollect.translate(-44, -50).rotateZ(360).opacity(1).step();

      animationTranspond.translate(-77, 0).rotateZ(360).opacity(1).step();

      animationInput.translate(-44, 50).rotateZ(360).opacity(1).step();

      this.setData({

        animPlus: animationPlus.export(),

        animCollect: animationcollect.export(),

        animTranspond: animationTranspond.export(),

        animInput: animationInput.export(),

      })

    },

    //收回动画

    takeback: function () {

     

      //plus逆时针旋转

      var animationPlus = wx.createAnimation({

        duration: 500,

        timingFunction: 'ease-out'

      })

      var animationcollect = wx.createAnimation({

        duration: 500,

        timingFunction: 'ease-out'

      })

      var animationTranspond = wx.createAnimation({

        duration: 500,

        timingFunction: 'ease-out'

      })

      var animationInput = wx.createAnimation({

        duration: 500,

        timingFunction: 'ease-out'

      })

      animationPlus.rotateZ(0).step();

      animationcollect.translate(0, 0).rotateZ(0).opacity(0).step();

      animationTranspond.translate(0, 0).rotateZ(0).opacity(0).step();

      animationInput.translate(0, 0).rotateZ(0).opacity(0).step();

      this.setData({

        animPlus: animationPlus.export(),

        animCollect: animationcollect.export(),

        animTranspond: animationTranspond.export(),

        animInput: animationInput.export(),

      })
 
    },
  }
})
