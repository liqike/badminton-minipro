const app = getApp()
import event from '../../utils/event'

Component({
  properties: {
    show:{
      type: Boolean,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: false,
    },
    isBack: {
      type: Boolean,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: false,
    },
    logout: {
      type: Boolean,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: false,
    },
    isAdd: {
      type: Boolean,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: false,
    },
    isSearch: {
      type: Boolean,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: false,
    },
    city:{
      type: String,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: "上海市",
    },
    text: {
      type: String,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: "color: #000",
    },
    bgcolor: {
      type: String,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: "background-color: #fff;",
    },
    title: {
      type: String,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: "",
    },
    entitle: {
      type: String,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: "",
    },
    bgcolor: {
      type: String,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: "",
    },
    address: {
      type: String,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: "上海市浦东新区",
    },
    options: {   //navbarData   由父页面传递的数据，变量名字自命名
      type: Object,  //目前接受的类型包括：String, Number, Boolean, Object, 
      value: {city:"上海市",address:"上海市浦东新区"},
      observer: function (newVal, oldVal) { }
    }
  },
  data: {
    height: 0,
    //默认值  默认显示左上角
    options: {
      show: false,
      city:'',
      address:'',
      language: {},
    }
  },
  attached: function () {
    this.setLanguage();	// (1)
    event.on("languageChanged", this, this.setLanguage); // (2)
    // 定义导航栏的高度   方便对齐
    this.setData({
      height: app.globalData.height
    })

  },
  methods: {
    chooseCity: function (e) {
      var that = this
      // wx.chooseLocation({
      //   success: function (res) {
      //     var newOptions = that.data.options
      //     var address = res.address
      //     var name = res.name
      //     var speed = res.longitude
      //     var accuracy = res.accuracy
      //     newOptions.address = res.address
      //     console.log(newOptions)
      //     that.setData({
      //       options: newOptions
      //     })
      //   }
      // })
    },
    setLanguage() {
      this.setData({
        language: wx.T.getLanguage()
      });
    },
    _back:function(){
      wx.navigateBack({
        url: wx.getStorageSync("page")
      })
    },
    _add:function(){
      this.triggerEvent('add', {}, {})
    },
    _search:function(){
      this.triggerEvent('search', {}, {})
    },
    _logout(){
      this.triggerEvent('logout', {}, {})
    }
  }

}) 