// components/cotent-title/index.js
Component({
  options: {
    addGlobalClass: true,
  },
  externalClasses: ['l-content'],
  /**
   * 组件的属性列表
   */
  properties: {
    name: {
      type: String
    },
    cardPadding: {
      type: Boolean,
      value: true
    },
    dot: {
      type: Boolean,
      value: true
    },
    code:{
      type: Boolean,
      value: false
    }
  },

  /**
   * 组件的初始数据
   */
  data: {

  },

  /**
   * 组件的方法列表
   */
  methods: {
    getqrcode(){
      this.triggerEvent('getqrcode', {}, {})
    }
  }
})