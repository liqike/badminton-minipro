var app = getApp();
const regeneratorRuntime = require("../regenerator/runtime");

function wxPromisify(fn) {
  return function (obj = {}) {
    return new Promise((resolve, reject) => {
      obj.success = function (res) {
        //成功
        resolve(res)
      }
      obj.fail = function (res) {
        //失败
        reject(res)
      }
      fn(obj)
    })
  }
}
//无论promise对象最后状态如何都会执行
Promise.prototype.finally = function (callback) {
  let P = this.constructor;
  return this.then(
    value => P.resolve(callback()).then(() => value),
    reason => P.resolve(callback()).then(() => { throw reason })
  );
};


/**
 * 微信请求get方法
 * url
 * data 以对象的格式传入
 */
function getRequest(url, data) {
  var getRequest = wxPromisify(wx.request)
  return getRequest({
    url: app.globalData.host + url,
    method: 'GET',
    data: data,
    header: {
      'Content-type': 'application/x-www-form-urlencoded',// 默认值,
      'Authentication': wx.getStorageSync("token")
    },
  })
}

function uploadFile(url, filePath, formData){
  var uploadFile = wxPromisify(wx.uploadFile)
  return uploadFile({
    // 模拟https
    url: app.globalData.host + url, //需要用HTTPS，同时在微信公众平台后台添加服务器地址  
    filePath: filePath, //上传的文件本地地址    
    name: 'file',
    formData: formData,
    header: {
      'Content-type': 'multipart/form-data',
      'Authentication': wx.getStorageSync("token")
    }
  })
}




/**
 * 微信请求post方法封装
 * url
 * data 以对象的格式传入
 */
function postRequest(url, data) {
  var postRequest = wxPromisify(wx.request)
  return postRequest({
    url: app.globalData.host + url,
    method: 'POST',
    data: data,
    header: {
      'Content-type': 'application/x-www-form-urlencoded',// 默认值,
      'Authentication': wx.getStorageSync("token")
    },
  })
}
/**
 * 微信请求post方法封装
 * url
 * data 以对象的格式传入
 */
function postJsonRequest(url, data) {
  var postRequest = wxPromisify(wx.request)
  return postRequest({
    url: app.globalData.host + url,
    method: 'POST',
    data: data,
    header: {
      'Content-type': 'application/json',// 默认值,
      'Authentication': wx.getStorageSync("token")
    },
  })
}
/**
 * 微信请求delete方法
 * url
 * data 以对象的格式传入
 */
function deleteRequest(url, data) {
  var getRequest = wxPromisify(wx.request)
  return getRequest({
    url: app.globalData.host + url+data,
    method: 'DELETE',
    header: {
      'Content-type': 'application/x-www-form-urlencoded',// 默认值,
      'Authentication': wx.getStorageSync("token")
    },
  })
}

/**
 * 微信请求put方法
 * url
 * data 以对象的格式传入
 */
function putRequest(url, data) {
  var getRequest = wxPromisify(wx.request)
  return getRequest({
    url: app.globalData.host + url,
    method: 'PUT',
    data: data,
    header: {
      'Content-type': 'application/x-www-form-urlencoded',// 默认值,
      'Authentication': wx.getStorageSync("token")
    },
  })
}
/**
 * 微信请求putJson方法
 * url
 * data 以对象的格式传入
 */
function putJsonRequest(url, data) {
  var getRequest = wxPromisify(wx.request)
  return getRequest({
    url: app.globalData.host + url,
    method: 'PUT',
    data: data,
    header: {
      'Content-type': 'application/json',// 默认值,
      'Authentication': wx.getStorageSync("token")
    },
  })
}
/**
 * 刷新token
 */
function refurbishToken() {
  var user = wx.getStorageSync("userInfo")
  var postRequest = wxPromisify(wx.request)
  return postRequest({
    url: app.globalData.host + '/api/v1/wxLogin/refurbishToken',
    method: 'GET',
    data: {
      userId: user.userId,
    },
    header: {
      'Content-type': 'application/x-www-form-urlencoded',// 默认值,
      'mobile': 't'
    },
  })


}
//封装小程序冷更新
function upDateSmallProgram() {
  wx.getSystemInfo({
    success: function (res) {
      var version = res.SDKVersion.slice(0, 5);
      console.log('res.SDKVersion', res.SDKVersion)
      version = version.replace(/\./g, "");
      if (parseInt(version) >= 199) {// 大于1.9.90的版本
        const updateManager = wx.getUpdateManager()

        updateManager.onCheckForUpdate(function (res) {
          // 请求完新版本信息的回调
          console.log("9999999", res.hasUpdate)
        })

        updateManager.onUpdateReady(function () {
          wx.showModal({
            title: '更新提示',
            showCancel: false,
            content: '版本已更新，请点击确定立即使用!',
            success: function (res) {
              if (res.confirm) {
                // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
                updateManager.applyUpdate()
              }
            }
          })

        })

        updateManager.onUpdateFailed(function () {
          // 新的版本下载失败

        })
      }
    }
  })
}


function showRequestError(res){
  // var statusCode = res.statusCode
  if (msg == undefined || msg == null) {
    msg = res.data.message
  }
  var code = res.data.resultCode
  var msg = res.data.resultMsg
  // if (res.statusCode != undefined && res.statusCode == 401) {
  //   this.refurbishToken().then(res => {
  //     var code = res.data.resultCode
  //     if (code === 20000) {
  //       wx.setStorageSync("token", res.data.result)
  //       var page = getCurrentPages().pop(); //获取当前页面实例
  //       if (page == undefined || page == null) return;
  //       page.onLoad(); //调用实例的onLoad方法重新加载数据;
  //     }
  //   })
if (code == 40006) {
    wx.showToast({
      title: msg,
      image: '../../img/bad-noauth.png',
      duration: 3000
    });
    setTimeout(function () {
      wx.navigateBack({
        url: wx.getStorageSync("page")
      })
    }, 700)
  }else{
    console.log(res)
    wx.showToast({
      title: msg,
      icon: 'none', 
      duration: 3000
    });
    setTimeout(function () {
      wx.navigateBack({
       delta:1
      })
    }, 3000)
  }
}
/**
 * 弹窗提示网络错误
 */
function showError() {
    wx.showModal({
      title: '加载失败',
      content: '请检查网络连接、服务器异常',
      showCancel: false,
    })
}

module.exports = {
  postRequest,
  getRequest,
  deleteRequest,
  putRequest,
  upDateSmallProgram,
  showError,
  showRequestError,
  postJsonRequest,
  putJsonRequest,
  refurbishToken,
  uploadFile
}