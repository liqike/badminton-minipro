const formatTime = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute].map(formatNumber).join(':')
}

const formatTime2 = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('-')
}

const formatDate = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  return [year, month, day].map(formatNumber).join('-')
}

const formatDateEN = date => {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()

  return year+"年"+month+"月"+day+"日"
}

const getAppointedDate = (now,optType, optPageNum)=> {
  // 　　let now = new Date(), //当前日期
    let 　nowDayOfWeek = now.getDay()//今天本周的第几天
    let　nowDay = now.getDate() //当前日
    let　nowMonth = now.getMonth() //当前月
    let nowYear = now.getFullYear() //当前年
    let　lastMonth = nowMonth - 1
  　　let startDate = '', endDate = '';
  　　if (optType == 1) {
    　　　　startDate = formatDateFn(new Date(nowYear, nowMonth, nowDay - nowDayOfWeek + 1 - (7 * optPageNum)))
    　　　　endDate = formatDateFn(new Date(nowYear, nowMonth, nowDay + 7 - nowDayOfWeek - (7 * optPageNum)))
  　　} else if (optType == 2) {
    　　　　startDate = this.formatDateFn(new Date(nowYear, lastMonth + 1 - (1 * optPageNum), 1))
    　　　　endDate = this.formatDateFn(new Date(nowYear, lastMonth + 1 - (1 * optPageNum), this.getMonthDays(nowYear, lastMonth + 1 - (1 * optPageNum))))
  　　}
  　　return { startDate: startDate, endDate: endDate }
}

const getMonthDays=(nowYear, myMonth)=> {
  　　let monthStartDate = new Date(nowYear, myMonth, 1);
  　　let monthEndDate = new Date(nowYear, myMonth + 1, 1);
  　　let days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24);
  　　return days;
}
const  formatDateFn=date=> {
  　　let myyear = date.getFullYear();
  　　let mymonth = date.getMonth() + 1;
  　　let myweekday = date.getDate();
  　　if (mymonth < 10) {
    　　　　mymonth = "0" + mymonth;
  　　}
  　　if (myweekday < 10) {
    　　myweekday = "0" + myweekday;
  　　}
  　　return (myyear + "-" + mymonth + "-" + myweekday);

}

const formatNumber = n => {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function getWeek(data){
  var weekday = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
  var mydate = new Date(data);
  var myddy = mydate.getDay();//获取存储当前日期
  return weekday[myddy]
}


/**
 * 代理小程序的 setData，在更新数据后，翻译传参类型的字符串
 *
 * @param {object} langData 当页的翻译模块
 */
function resetSetData(langData) {
  let self = this

  /**
   * 在小程序中，使用子组件的页面， this.setData configurable 和 writable 都是 false
   * 所以不能重置 setData 方法，只能另起一个函数名，这里用了 setComData，
   * 另外，由于在 langData.js 中 setTransData 方法调用的是 setData，
   * 所以，另外给使用子组件的页面，定义了个 setComTransData 方法，去调用 setComData
   */

  let isUsingComponents = !self.__proto__.hasOwnProperty('setData') && self.__proto__.__proto__.hasOwnProperty('setData')
  let _ = self.setData

  if (!isUsingComponents) {
    self.setData = function (data, isSetTrans = false) {
      _.call(self, data)
      if (isSetTrans) {/* 阻止翻译循环调用 setData  */return; }
      langData.setTransData && langData.setTransData.call(self)
    }
  } else {
    self.setComData = function (data, isSetTrans = false) {
      _.call(self, data)
      if (isSetTrans) {/* 阻止翻译循环调用 setData  */return; }
      langData.setComTransData && langData.setComTransData.call(self)
    }
  }

}

module.exports = {
  formatTime,
  formatDate,
  resetSetData,
  getWeek,
  formatDateEN,
  formatTime2,
  getAppointedDate
}
