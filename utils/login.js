var app = getApp()

function login(detail) {
  var that = this
  var userInfo = detail
  /** 
  var latitude =''
  var longitude =''
  wx.getLocation({
    type: 'wgs84',
    success: function (res) {
      latitude = res.latitude
      longitude = res.longitude
      console.log(latitude + "111111")
    }
  })
 */
 
 wx.login({
      success: res => {
        wx.request({
          url: app.globalData.host +'/api/v1/wxLogin/wxLogin',
          data: {
            wxCity: userInfo.city,
            wxNickname: userInfo.nickName,
            wxCountry: userInfo.country,
            wxImg: userInfo.avatarUrl,
            wxProvince: userInfo.province,
            wxGender: userInfo.gender,
            wxOpenid: wx.getStorageSync("openID"),
            userPhone: wx.getStorageSync("realphone"),
          },
          header: {
            'Content-type': 'application/json',// 默认值,
            'mobile': 't'
          },
          method: 'POST',
          success: function (res) {
            wx.hideLoading() 
            console.log(res)
            var code = res.data.resultCode
            if(code==20000){
              wx.setStorageSync("userInfo", res.data.result.wxUsers)
              wx.setStorageSync("token", res.data.result.token)
              wx.setStorageSync("role", res.data.result.roles[0])
              let pages = getCurrentPages(); //获取当前页面js里面的pages里的所有信息。
              let prevPage = pages[pages.length - 2];  
              prevPage.setData({  // 将我们想要传递的参数在这里直接setData。上个页面就会执行这里的操作。
                currentTab: -1,
                login: true
              })

              //上一个页面内执行setData操作，将我们想要的信息保存住。当我们返回去的时候，页面已经处理完毕。
              //最后就是返回上一个页面。
              wx.navigateBack({
                delta: 2  // 返回上一级页面。
              })
              
              // prevPage.refresh()
                // wx.reLaunch({
                //   url: wx.getStorageSync("page"),
                // }) 
            }
           
          },
          fail: function (res) {
            console.log('is failed')
          }
        })
      }
    })
}

function auth(){
  wx.request({
    url: app.globalData.host + '/api/v1/wxLogin/wxAuth',
    data: {
      openid: wx.getStorageSync("openID"),
    },
    header: {
      'Content-type': 'application/x-www-form-urlencoded',// 默认值,
      'mobile': 't'
    },
    method: 'POST',
    success: function (res) {
      if (res.data.result){
        login(wx.getStorageSync("userInfo"))
      }else{
        wx.navigateTo({
          url: '../roles/roles',
        })
      }
      
    },
    fail: function (res) {
      console.log('is failed')
    }
  })
}

function getSessionKey() {
  if (wx.getStorageSync("sessionKey") == '') {
    wx.login({
      success: rests => {
        wx.request({
          url: app.globalData.host + '/api/v1/wxLogin/loginWithCode',
          data: {
            code: rests.code,
          },
          header: {
            "Content-Type": "application/x-www-form-urlencoded",// 默认值,
            'mobile': 't'
          },
          method: 'POST',
          success: function (res) {
            var code = res.data.resultCode
            console.log(res)
            if (code === 20000) {
              wx.setStorageSync("openID", res.data.result.wxOpenid)
              wx.setStorageSync("sessionKey", res.data.result.sessionKey)

             
            }
          },
          fail: function (res) {
            console.log('is failed')
          }
        })
      }
    })
  } else {
    wx.navigateTo({
      url: '/pages/phone/phone'
    })

  }
    
  }
// }
module.exports = {
  login: login,
  auth: auth,
  getSessionKey: getSessionKey
}


/**
* 生命周期函数--监听页面初次渲染完成
*/
