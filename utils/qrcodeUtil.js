const request = require('../utils/requestApi')
/**
 * 获取分享二维码 并展示
 */
function getqrcode(data) {
  var codeUrl = "/api/v1/personal/getCode"
  //生成二维码 请求
  request.postRequest(codeUrl, data).then(res => {
    wx.hideLoading()
    console.log(res)
    if (res.data.resultCode == 20000) {
    let base64Url = "data:image/png;base64," + res.data.result;
    var images = []
    images.push(base64Url)
    wx.previewImage({
      //当前显示下表
      current: images[0],
      //数据源
      urls: images
    })
      }
  })
}

// wx.request({
//   //获得二维码数据接口
//   url: 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token=' + res.data.result,
//   data: {
//     'scene': scene,//必填 scene 携带参数，可以在onload页面里面query里面解析出来，详情百度
//     'page': page,//必填 具体的小程序页面，但不能携带参数，参数在scene中携带   color: rgb(53, 156, 122);
//     'line_color': { 'r': 53, 'g': 156, 'b': 122 },//小程序二维码线条颜色
//     'is_hyaline': false,
//     'width': 430
//   },
//   header: {
//     'content-type': 'application/json'
//   },
//   method: 'POST',
//   // dataType: 'json',
//   responseType: 'arraybuffer',//将原本按文本解析修改为arraybuffer
//   success: function (res) {
//     wx.hideLoading()
//     let base64Url = "data:image/png;base64," + wx.arrayBufferToBase64(res.data);
//     var images = []
//     images.push(base64Url)
//     wx.previewImage({
//       //当前显示下表
//       current: images[0],
//       //数据源
//       urls: images
//     })

//   }

// })

module.exports = {
  getqrcode
}