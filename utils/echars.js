


function getline(data){
 var  option = {
    title: {
      text: '今日',
      left: '50%',
      textAlign: 'center'
    },
    tooltip: {
      trigger: 'axis',
      axisPointer: {
        lineStyle: {
          color: '#ddd'
        }
      },
      backgroundColor: 'rgba(255,255,255,0.5)',
      padding: [5, 10],
      textStyle: {
        color: '#7588E4',
      },
      extraCssText: 'box-shadow: 0 0 5px rgba(0,0,0,0.3)'
    },
    dataZoom: [{
      type: 'slider',
      xAxisIndex: 0,
      height: '15',
      bottom: '55',
      show: true,
      start: 0,
      end: 100,
      handleStyle: { /*手柄的样式*/
        width: '20',
        color: "#085ABF",
        borderColor: "#085ABF"
      },
      backgroundColor: "#f7f7f7", /*背景 */
      dataBackground: { /*数据背景*/
        lineStyle: {
          color: "#dfdfdf"
        },
        areaStyle: {
          color: "#dfdfdf"
        }
      },
      fillerColor: "#FFEFBE", /*被start和end遮住的背景*/
      labelFormatter: function (value, params) { /*拖动时两端的文字提示*/
        var str = "";
        if (params.length > 4) {
          str = params.substring(0, 4) + "…";
        } else {
          str = params;
        }
        return str;
      }
    }],
    legend: {
      right: 20,
      orient: 'vertical',
      data: ['今日']
    },
    xAxis: {
      type: 'category',
      name: '日期',
      data: ['00:00', '2:00', '4:00', '6:00', '8:00', '10:00', '12:00', '14:00', '16:00', '18:00', '20:00', "22:00"],
      boundaryGap: false,
      splitLine: {
        show: false,
        interval: 'auto',
        lineStyle: {
          color: ['#EEEEEE']
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#50697A'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          color: '#50697A',
          fontSize: 13
        }
      }
    },
    yAxis: {
      type: 'value',
      name: '数量（件）',
      splitLine: {
        lineStyle: {
          color: ['#EEEEEE']
        }
      },
      axisTick: {
        show: false
      },
      axisLine: {
        lineStyle: {
          color: '#50697A'
        }
      },
      axisLabel: {
        margin: 10,
        textStyle: {
          color: '#50697A',
          fontSize: 13
        }
      }
    },
    series: [{
      name: '今日',
      type: 'line',
      smooth: true,
      showSymbol: false,
      symbol: 'circle',
      symbolSize: 6,
      data: ['1200', '1400', '808', '811', '626', '488', '1600', '1100', '500', '300', '1998', '822'],
      areaStyle: {
        normal: {
          color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
            offset: 0,
            color: 'rgba(16, 79, 193,1)'
          }, {
            offset: 1,
            color: 'rgba(125, 178, 244,1)'
          }], false)
        }
      },
      itemStyle: {
        normal: {
          color: 'rgba(16, 79, 193,1)'
        }
      },
      lineStyle: {
        normal: {
          width: 0
        }
      }
    }, {
      name: '折线',
      type: 'line',
      symbol: 'circle',
      symbolSize: 10,
      data: ['1200', '1400', '808', '811', '626', '488', '1600', '1100', '500', '300', '1998', '822'],
      itemStyle: {  //折线拐点的样式
        normal: {
          color: '#FAC220',  //折线拐点的颜色
        }
      },
      lineStyle: {  //线条的样式
        normal: {
          color: '#FAC220',  //折线颜色
        }
      }
    }]
  };
  return option
}
/**
 *  饼状图 [
          { value: 335, name: '直接访问' },
          { value: 310, name: '邮件营销' }
        ]
 */
function getPeih(data){

    // 指定图表的配置项和数据
var option = {
series: [{
    type: "pie",                // 系列1类型: 饼图
    center: ["50%","50%"],      // 饼图的中心(圆心)坐标，数组的第一项是横坐标，第二项是纵坐标。[                 default: ['50%', '50%'] ]
    radius: ["60%","60%"],      // 饼图的半径，数组的第一项是内半径，第二项是外半径。[ default: [0, '75%'] ]
                                // 可以将内半径设大显示成圆环图（Donut chart）。
    clockWise: true,           // 饼图的扇区是否是顺时针排布。[ default: true ]
    startAngle: 90,             // 起始角度，支持范围[0, 360]。 [ default: 90 ]
    hoverAnimation: false,       // 是否开启 hover 在扇区上的放大动画效果。[ default: true ]
	  itemStyle: {                // 图形样式
      normal: {
        color: "rgba(10, 138, 21, 0.932)",       // 图形的颜色
        borderColor: "rgba(10, 138, 21, 0.932)", // 图形的描边颜色
      borderWidth: 10,        // 描边线宽。为 0 时无描边。[ default: 0 ]
      borderType: 'solid',    // 柱条的描边类型，默认为实线，支持 'solid', 'dashed', 'dotted'。
      label: {                // 图形内部标签
        show: true,                 // 是否显示标签
        textStyle: {                // 标签文本样式
          fontSize: 12,
          fontWeight: "bold"      // 标签字体加粗,'normal','bold','bolder','lighter',100 | 200 | 300 | 400...
     },
    position: "center"          // 标签的位置,'outside'(饼图扇区外侧，通过视觉引导线连到相应的扇区)
                            // 'inside'(饼图扇区内部);  'inner' 同 'inside'。
                            // 'center'(在饼图中心位置。)
    },
	  labelLine: {            // 标签的视觉引导线样式，在 label 位置 设置为'outside'的时候会显示视觉引导线。
      show: false
     }
    },
	  // emphasis: {                 // 高亮的扇区和标签样式(起强调作用)
    // color: "#5886f0",
    // borderColor: "#5886f0",
    // borderWidth: 15,
    // borderType: 'solid',
	  // label: {
    //   textStyle: {
    //   fontSize: 10,
    // fontWeight: "bold"
    //         }
    //      }
    //    }
},
  data: [{ value: data, name: "胜率" + '\n' + data+"%"},
    {
      name: " ", value: 100 - data,
	    itemStyle: {
          normal: {
            color: "rgba(10, 138, 21, 0.932)",
            borderColor: "rgba(10, 138, 21, 0.932)",
          borderWidth: 2,
          label: {
              show: false
          },
          labelLine: {
              show: false
          }
        },
	  // emphasis: {
    //     color: "#5886f0",
    //     borderColor: "#5886f0",
    //     borderWidth: 5
    //   }
    }
}]
}
]};
return option
}

module.exports = {
  getline,
  getPeih
}