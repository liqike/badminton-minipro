var QQMapWX = require('../libs/qqmap-wx-jssdk.min.js');
const regeneratorRuntime = require("../regenerator/runtime");
var qqmapsdk;

function init() {
  qqmapsdk = new QQMapWX({
    key: 'AODBZ-LDIWX-4C345-ZKR7I-PM565-HEB6X'
  });
}

function getUserLocation() {
  return new Promise((resolve, reject) => {
    init()
    let vm = this;
    wx.getSetting({
      success: (res) => {
        // res.authSetting['scope.userLocation'] == undefined    表示 初始化进入该页面
        // res.authSetting['scope.userLocation'] == false    表示 非初始化进入该页面,且未授权
        // res.authSetting['scope.userLocation'] == true    表示 地理位置授权
        if (res.authSetting['scope.userLocation'] != undefined && res.authSetting['scope.userLocation'] != true) {
          wx.showModal({
            title: '请求授权当前位置',
            content: '需要获取您的地理位置，请确认授权',
            success: function (res) {
              if (res.cancel) {
                wx.showToast({
                  title: '拒绝授权',
                  icon: 'none',
                  duration: 1000
                })
              } else if (res.confirm) {
                wx.openSetting({
                  success: function (dataAu) {
                    if (dataAu.authSetting["scope.userLocation"] == true) {
                      wx.showToast({
                        title: '授权成功',
                        icon: 'success',
                        duration: 1000
                      })
                      //再次授权，调用wx.getLocation的API
                      vm.getLocation().then(data => {
                        resolve(data)
                      })
                    } else {
                      wx.showToast({
                        title: '授权失败',
                        icon: 'none',
                        duration: 1000
                      })
                    }
                  }
                })
              }
            }
          })
        } else if (res.authSetting['scope.userLocation'] == undefined) {
          //调用wx.getLocation的API
          vm.getLocation().then(data => {
            resolve(data)
          })
        }
        else {
          //调用wx.getLocation的API
          var res = vm.getLocation().then(data => {
            resolve(data)
          })
        }
      },
      fail: function (res) {
        reject(res);
        wx.showToast({
          title: "网络异常！",
          icon: 'none',
          duration: 3000
        });
      }
    })
  })
}

// 微信获得经纬度
function getLocation() {
  return new Promise((resolve, reject) => {
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {

        var latitude = res.latitude
        var longitude = res.longitude
        var speed = res.speed
        var accuracy = res.accuracy;
        getLocal(latitude, longitude)
          .then(data => {
            resolve(data)
          })
      },
      fail: function (res) {
        reject(res);
        console.log('fail' + JSON.stringify(res))
        wx.showModal({
          title: '',
          content: '请在手机系统设置中开启定位服务',
          // confirmText: '确定',
          success: function (res) {
          }
        })     
      }
    })
  })
}
// 获取当前地理位置
function getLocal(latitude, longitude) {
  return new Promise((resolve, reject) => {
    qqmapsdk.reverseGeocoder({
      location: {
        latitude: latitude,
        longitude: longitude
      },
      success: function (res) {
        console.log(res);

        let province = res.result.ad_info.province
        let city = res.result.ad_info.city
        let address = res.result.address
        var maps = []
        var options = {}
        options.province = province
        options.city = city
        options.address = address
        options.latitude = latitude //纬度
        options.longitude = longitude //经度
        const markers = getMarkersNotWithName(latitude, longitude, address)
        maps.push(options)
        maps.push(markers)
        resolve(maps)
      },
      fail: function (res) {
        reject(res);
      },
      complete: function (res) {
        // console.log(res);
      }
    })
  })
}

function juli(lat1, lng1, lat2, lng2) {
  // console.log(lat1, lng1, lat2, lng2)
  var radLat1 = lat1 * Math.PI / 180.0;
  var radLat2 = lat2 * Math.PI / 180.0;
  var a = radLat1 - radLat2;
  var b = lng1 * Math.PI / 180.0 - lng2 * Math.PI / 180.0;
  var s = 2 * Math.asin(Math.sqrt(Math.pow(Math.sin(a / 2), 2) + Math.cos(radLat1) * Math.cos(radLat2) * Math.pow(Math.sin(b / 2), 2)));
  s = s * 6378.137;
  s = Math.round(s * 10000) / 10000;
  return s
}
/**
 * 获取地图标识带name
 */
function getMarkersWithName(latitude, longitude, name, address) {
  const markers = [{
    id: "1",
    latitude: latitude,
    longitude: longitude,
    callout: {
      content: "名称：" + name + "\n\r" + "当前地址：" + address,
      color: '#333',
      fontSize: 13,
      bgColor: '#fff',
      padding: 10,
      borderRadius: 3,
      display: 'ALWAYS'
    },
    width: 30,
    height: 30,
    iconPath: "../../img/marker.png"
  }]
  return markers
}
/**
 * 获取地图标识不带name
 */
function getMarkersNotWithName(latitude, longitude, address) {
  const markers = [{
    id: "1",
    latitude: latitude,
    longitude: longitude,
    callout: {
      content: "当前地址：" + address,
      color: '#333',
      fontSize: 13,
      bgColor: '#fff',
      padding: 10,
      borderRadius: 3,
      display: 'ALWAYS'
    },
    width: 30,
    height: 30,
    iconPath: "../../img/marker.png"
  }]
  return markers
}


module.exports = {
  init: init,
  getUserLocation: getUserLocation,
  getLocation: getLocation,
  juli: juli,
  getMarkersNotWithName,
  getMarkersWithName
}